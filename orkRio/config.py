# coding: utf-8

import os
from tornado.options import define, options, parse_command_line
from orkit.utils.pgutils import DB

ROOTPATH = os.path.dirname(os.path.abspath(__file__))
DEFAULT_LANGUAGE = 'fr'

define("dbuser", default='orika', help="db user", type=str)
define("dbpwd", default='orika', help="db password", type=str)
define("dbhost", default='localhost', help="db host", type=str)
define("dbport", default=5432, help="db port", type=int)
define("dbname", default='orktest', help="db name", type=str)
define("http_port", default=10000, help="run on the given port", type=int)
define("host", default='0.0.0.0', help="access for everyone", type=str)

parse_command_line()

db = DB(options.dbuser, options.dbpwd, options.dbname, options.dbhost, options.dbport)

