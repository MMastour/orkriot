<data-table>
    <!--TABLE-->
    <div  if={!exist}>
    <table id="mytable" class="ui celled table" >
        <thead>
             <tr id="tableHeader">
                <th>Semaine
                    <div><i id="ids" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="ids" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon  {triverif:asc}"></i></div>
                </th>
                <th>CA
                    <div><i id="ca" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="ca" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Panier Moyen
                    <div><i id="pm" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="pm" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Nb Client
                    <div><i id="ncli" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="ncli" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Commentaire
                    <div><i id="comment" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="comment" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Visé
                    <div><i id="vise" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="vise" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Date Création
                    <div><i id="dt" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="dt" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Date Modification
                    <div><i id="dtm" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="dtm" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
                <th>Utilisateur
                    <div><i id="usr" onclick="{takeIdTri('desc')}" class="sort Attributes descending icon"></i>
                    <i id="usr" onclick="{takeIdTri('asc')}" class="sort Attributes ascending icon"></i></div>
                </th>
             </tr>
         </thead>
                <!--Afficher les éléments après la première consultation-->
        <tbody>
             <tr each={ opts.items }>
                <form name="formulaire">
                    <input type="hidden" id="idobj" name="idobj" value="{idobj}" />
                        
                    <td>{ids}</td>
                    <td>{ca}</td>
                    <td>{pm}</td>
                    <td>{ncli}</td>
                    <td>{comment}</td> 
                    <td>{vise}</td> 
                    <td>{dt}</td>
                    <td>{dtm}</td>
                    <td>{usr}</td>
                </form>
            </tr> 
        </tbody>
    </table>
    </div>
            
                <h1 if = {exist} class="titre">Veuillez commencer par insérer les objectifs de l'année!</h1>   
         

    <script>
    var donnees = opts.items;

        if (donnees.length == 0)
            {exist = true}
        else
            {exist = false}


//cette fonction déclenche le traitement de tri
takeIdTri(verifTri) {
  return function(e) {
    an = searchAn.value;
    enseigne = searchEns.value;
    magasin = searchMag.value;
   

    if (verifTri == 'asc')
        order = 'ASC';
    else 
        order = 'DESC';
        myId = e.target.id;
      var data = {
            /*Les valeurs à envoyer au serveur*/
            /*Le critère de recherche soit par id de semaine, chiffre d'affaire  pannier moyen .....*/
            critere: myId,
            /*l'ordre de recherche*/
            order : order,
            an : an,
            enseigne : enseigne,
            magasin : magasin
        };
        var dataToSend = JSON.stringify(data);
        $.ajax({
                url: '/tri'
                , type: 'POST'
                ,data : dataToSend
                , dataType : 'json'
                , success: function (jsonResponse) {
                    var items = jsonResponse;
                    
                riot.mount('data-table', {items: items });

                    }
                
                , error: function () {
                    $("#tbody").text("Error to load api");
                }
            });
      }
  }

    </script>

</data-table>