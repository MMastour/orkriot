<flot-chart>

	<div class="demo-container">
      <img id="closeGraphes" class="iconeClose" src= "/static/images/close.png" onclick="{fermer}">
	 
		<div id="placeholder" class="demo-placeholder"></div>
	</div>

	<script>
	this.on('mount', function(){

		
		$.plot(("#placeholder"), opts.data, {
              xaxis: {
                        min: 1
                        , max: 52
                        , tickSize: 1},     
              lines: {
              show: true,
                        fillColor: {
                            colors: [{ opacity: 1 }, { opacity: 1 } ]}},
            points: {
              show: true,
                        fillColor: '#bc6bff'},
              grid: {
                        hoverable: true,
                        clickable: true}
                });

		});

// Bouton fermer unmout tout le tag
 this.fermer = function (e){
  this.unmount(true);
 }

	</script>



</flot-chart>