<menu-tag>

  
  
  
  <div class="ui pointing menu">
    <!-- Graphe -->
    <a class="item" onclick="{lancerGraphe}">
      GRAPHE
    </a>
    <!-- Recherche -->
    <div class="right menu">
          <!-- Champ Année -->
           <div class="autoComplete">
            <div class="item">
                  <div class="ui transparent icon input">
                    <input id="searchAn" placeholder="Année..." type="text" value="{opts.inputs[0]}" onkeyup="{dropSearch('an')}"></<input>
                  </div>
            </div>
            <!-- Liste des valeurs années reçues -->
            <div if={ cacheAn } id="dropAn" class="drop-menu" >
              <ul>
                <li each={an in items} onclick="{takeValue(an['an'],'an')}">
                  {an['an']}
                </li>
              </ul>
            </div>
          </div>
          
          <!-- Champ Enseigne -->
           <div class="autoComplete">
            <div class="item">
                  <div class="ui transparent icon input">
                    <input id="searchEns" placeholder="Enseigne..." type="text" value="{opts.inputs[1]}" onkeyup="{dropSearch('ens')}"></<input>
                  </div>
            </div>
            <!-- Liste des valeurs enseigne reçues -->
            <div if={ cacheEns } class="drop-menu">
              <ul>
                <li id="idenseigne" each={ens in items} value="{ens['idens']}" onclick="{takeValue(ens['nom'],'ens')}">
                  {ens['nom']}
                </li>
              </ul>
            </div>
          </div>
          
          <!-- Champ Magasin -->
           <div class="autoComplete">
            <div class="item">
                  <div class="ui transparent icon input">
                    <input id="searchMag" placeholder="Magasin..." type="text" value="{opts.inputs[2]}" onkeyup="{dropSearch('mag')}"></<input>
                    <i class="search link icon"  onclick="{rechercher}"></i>
                  </div>
            </div>
            <!-- Liste des valeurs magasins reçues -->
            <div if={ cacheMag } class="drop-menu">
              <ul>
                <li id="idmagasin" each={mag in items}  value="{mag['idmag']}" onclick="{takeValue(mag['nom'],'mag')}">
                  {mag['nom']}
                </li>
              </ul>
            </div>
          </div>

    </div>
  </div>



  <script>

// valeurs par defaut qui seront changées après chaque recherche
var idEns = 2;
var idMag = 5;

// Fonction pour afficher le graphe
this.lancerGraphe = function (e) {
      // Prendre les valeurs dans les inputs pour afficher le graphe selon les critères de la recherche
      // Préparation des données sous forme Json avant l'envoie
        an = this.searchAn.value;
        enseigne = this.searchEns.value;
        magasin = this.searchMag.value; 
        var data = {an : an,
                  enseigne : enseigne,
                  magasin : magasin
          };
        var dataToSend = JSON.stringify(data);

        $.ajax({
            url: '/graphe'
            , type: 'POST'
            ,data:dataToSend
            , dataType: 'json'
                /*Récupérer les données json dans objresponse*/
                
            , success: function (jsonResponse) {
                
                var objresponse = jsonResponse;
                var data = [{data: objresponse, color: "#4c1876",label: "Chiffre d'affaire par semaine"}];
                // Après la réception des des données depuis le serveur il faut les envoyer au tag flot-chart
                // riot.mount permet l'envoit des données et afficher le contenu du tag
                riot.mount('flot-chart',{data : data});

                
            }
        });
},

// Fonction pour activer l'autocomplete après chaque click
dropSearch(condition) {
  return function(e) {
    // Prendre la valuer de l'input après chaque tape
      var valeur = e.target.value;
      // Condition est l'argument de la fonction qui va nous permetre de savoir sur quel champ on écrit
      // idEns va nous permettre de récupérer le magasin, c'est cet id qui sera utilisé à la place du critère
      var data = {critere : valeur,
                  condition : condition,
                  idEns : idEns
      };
        var dataToSend = JSON.stringify(data);

    $.ajax({
            url: '/datacomplete'
            ,type: 'POST'
            ,data: dataToSend
                /*Récupérer les données json dans objresponse*/
            , success: function (jsonResponse) {
                
                items = JSON.parse(jsonResponse);

                // Afficher les listes selon le champ souhaité
                if (condition == 'an')
                {
                      if((items.length == 0 || !valeur) && condition == 'an')
                        {cacheAn = false }
                      else
                        {cacheAn = true }
                }
                    
                else if (condition == 'ens')
                {
                      if((items.length == 0 || !valeur) && condition == 'ens')
                        {cacheEns = false }
                      else
                        {cacheEns = true }
                }
                                        
                else
                {
                      if((items.length == 0 || !valeur) && condition == 'mag')
                        {cacheMag = false }
                      else
                        {cacheMag = true }
                }
                      
            }
        });
  }
}

// Fonction pour prendre la valeur depuis la liste après click
// ValeurChoisie c'est la valeur qui sera mit dans l'input passée comme paramettre
// idEns et idMag seront remplis par les ids récupérés depuis la base 
takeValue(valeurChoisie,condition) {
  return function(e) {
    
    if(condition == 'an')
    {
      this.searchAn.value = valeurChoisie;
        cacheAn = false;
    }

    else if(condition == 'ens')
    {
      this.searchEns.value = valeurChoisie;
      idEns = e.target.value;
      this.searchMag.value = '';
        cacheEns = false;
    }

    else
    {
      this.searchMag.value = valeurChoisie;
      idMag = e.target.value;
        cacheMag = false;
    }

    }
  }

// Fonction pour lancer le traitement de la recherche selon les trois critères
  this.rechercher = function (e){

    inputValueAn = this.searchAn.value;
     var data = {
            /*La valeur à envoyer au serveur*/
            an: inputValueAn,
            ens: idEns,
            mag: idMag
        };
        var dataToSend = JSON.stringify(data);
        $.ajax({
                url: '/recherche'
                , type: 'POST'
                ,data : dataToSend
                , dataType : 'json'
                , success: function (jsonResponse) {
                    var items = jsonResponse;
                    // changement des données dans la table
                    
                riot.mount('data-table', {items: items });

                    }
                
                , error: function () {
                    $("#tbody").text("Error to load api");
                }
            });

  }

  </script>

</menu-tag>