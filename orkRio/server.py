#!/usr/bin/env python
# coding: utf-8

import logging
import os
import traceback

import tornado.ioloop
import tornado.web
from tornado.options import options
from tornado.web import gen

import config
from config import db
import json
from  tornado.escape import json_decode
from  tornado.escape import json_encode

from orkit.handlers.wssrvhandler import BaseHandler
import datetime

now = datetime.datetime.now()
ROOT_PATH = os.path.dirname((__file__))


def date_handler(obj):
	if hasattr(obj, 'isoformat'):
		return obj.isoformat()



class ObjectifsHandler(BaseHandler):

	@gen.coroutine
	def get(self, path=None):
		logging.info(self.__class__.__name__+".get(path=%s)" % path)
		try:
			#affichage des données par défaut
			if path == "home":
				
				items = yield db.select("SELECT * FROM objectifs LIMIT 1")
				if items:
					an = items[0]['an']
					idens = items[0]['idens']
					idmag = items[0]['idmag']

					enseigne = yield db.select("SELECT nom FROM enseigne where idens = "+str(idens))
					magasin = yield db.select("SELECT nom FROM magasin where idens = "+str(idens)+" and idmag = "+str(idmag))

					items = yield db.select("SELECT * FROM objectifs where an = "+str(an)+" and idens = "+str(idens)+" and idmag = "+str(idmag)+" order by ids")
					

					#SERIALISATION DU DECIMAL
					for it in items:
							it['ca'] =float(it['ca'])
							it['pm'] =float(it['pm'])
							it['dtm']=str(it['dtm'])
							it['dt']=str(it['dt'])
					# Afficher 52 lignes
					dataTable = []
					for i in range(1,53):
						j=0
						for item in items: 
							if item['ids'] == i:
								dataTable.append(item)
								j=1
								break

						if j == 0:
							dataDict = {}
							dataDict['ids'] = i
							dataDict['ca'] = ''
							dataDict['pm'] = ''
							dataDict['ncli'] = ''
							dataDict['comment'] = ''
							dataDict['vise'] = ''
							dataDict['dt'] = ''
							dataDict['dtm'] = ''
							dataDict['usr'] = ''
							dataTable.append(dataDict)

					dataInput = [an,enseigne[0]['nom'],magasin[0]['nom']]
					tableToSend = [dataTable,dataInput]
					self.write(json.dumps(tableToSend))
					
				else:
					items = [[],[]]
					self.write(json.dumps(items))

				


			
			else:
				
				self.render("index.html")
		except:
			traceback.print_exc()

	@gen.coroutine
	def post(self, path=None):
		logging.info(self.__class__.__name__+".get(path=%s)" % path)
		try:
			#le traitement de tri
			if path == "tri":
				json_obj = json_decode(self.request.body)
				critere = json_obj['critere']
				order = json_obj['order']
				an = json_obj['an']
				enseigne = json_obj['enseigne']
				magasin = json_obj['magasin']
				#
				if critere != "ids":
					
					enseignes = yield db.select("SELECT idens FROM enseigne where nom = '"+enseigne+"'")
					idens = enseignes[0]['idens']
					magasins = yield db.select("SELECT idmag FROM magasin where nom = '"+magasin+"' and idens = "+str(idens))
					idmag = magasins[0]['idmag']
					items = yield db.select("SELECT * FROM objectifs where an = "+str(an)+" and idens = "+str(idens)+" and idmag = "+str(idmag)+" order by "+critere+ " "+order)
					
					#SERIALISATION DU DECIMAL
					for it in items:
						it['ca'] =float(it['ca'])
						it['pm'] =float(it['pm'])
						it['dtm']=str(it['dtm'])
						it['dt']=str(it['dt'])
					# Afficher 52 lignes
					dataTable = []
					for item in items:
						dataTable.append(item)

					
					for i in range(1,53):
						j=0
						for item in items: 
							if item['ids'] == i:
								j=1
								break

						if j == 0:
							dataDict = {}
							dataDict['ids'] = i
							dataDict['ca'] = ''
							dataDict['pm'] = ''
							dataDict['ncli'] = ''
							dataDict['comment'] = ''
							dataDict['vise'] = ''
							dataDict['dt'] = ''
							dataDict['dtm'] = ''
							dataDict['usr'] = ''
							dataTable.append(dataDict)

					self.write(json.dumps(dataTable))
				else:
					enseignes = yield db.select("SELECT idens FROM enseigne where nom = '"+enseigne+"'")
					idens = enseignes[0]['idens']
					magasins = yield db.select("SELECT idmag FROM magasin where nom = '"+magasin+"' and idens = "+str(idens))
					idmag = magasins[0]['idmag']
					items = yield db.select("SELECT * FROM objectifs where an = "+str(an)+" and idens = "+str(idens)+" and idmag = "+str(idmag)+" order by "+critere+ " "+order)

					for it in items:
						it['ca'] =float(it['ca'])
						it['pm'] =float(it['pm'])
						it['dtm']=str(it['dtm'])
						it['dt']=str(it['dt'])
					# Afficher 52 lignes
					dataTable = []
					for i in range(1,53):
						j=0
						for item in items: 
							if item['ids'] == i:
								dataTable.append(item)
								j=1
								break

						if j == 0:
							dataDict = {}
							dataDict['ids'] = i
							dataDict['ca'] = ''
							dataDict['pm'] = ''
							dataDict['ncli'] = ''
							dataDict['comment'] = ''
							dataDict['vise'] = ''
							dataDict['dt'] = ''
							dataDict['dtm'] = ''
							dataDict['usr'] = ''
							dataTable.append(dataDict)

					if order == "DESC":
						dataTable = list(reversed(dataTable))

					self.write(json.dumps(dataTable))

            #traitement de graphe
			if path == "graphe":
				json_obj = json_decode(self.request.body)
				an = json_obj['an']
				enseigne = json_obj['enseigne']
				magasin = json_obj['magasin']
				enseignes = yield db.select("SELECT idens FROM enseigne where nom = '"+enseigne+"'")
				idens = enseignes[0]['idens']
				magasins = yield db.select("SELECT idmag FROM magasin where nom = '"+magasin+"' and idens = "+str(idens))
				idmag = magasins[0]['idmag']
				items = yield db.select("SELECT DISTINCT ids,ca FROM objectifs where an = "+str(an)+" and idens = "+str(idens)+" and idmag = "+str(idmag)+" order by ids")	
				#SERIALISATION DU DECIMAL
				for it in items:
					it['ca'] =float(it['ca'])

				temp = []
				myList = []
				i=0
				# Transformation en format adaptable au graphe
				for item in items:
					temp =[items[i]['ids'],items[i]['ca']]
					myList.append(temp)
					i+=1
				#L'envoie Json
				print myList
				self.write(json.dumps(myList))		

            #le traitement d'auto-complete des inputs de recherche
			if path == "datacomplete":
				json_obj = json_decode(self.request.body)
				critere = json_obj['critere']
				condition = json_obj['condition']
				idEns = json_obj['idEns']
				dataTable = []
				if condition == 'an':
					items = yield db.select("SELECT DISTINCT an FROM objectifs where an ::TEXT like '%s%%'"%critere)
					for item in items:
						dataTable.append(item['an'])
				elif condition == 'ens':
					items = yield db.select("SELECT DISTINCT * FROM enseigne where LOWER(nom) ::TEXT like LOWER('%s%%')"%critere)
				else:
					items = yield db.select("SELECT DISTINCT * FROM magasin where idens ::TEXT like '%s%%' and LOWER(nom) ::TEXT like LOWER('%s%%')"%(idEns,critere))
					
				
				self.write(json.dumps(items))

			#le traitement de la recherche 
			if path == "recherche":
				json_obj = json_decode(self.request.body)
				an = json_obj['an']
				ens = json_obj['ens']
				mag = json_obj['mag']
				
				if an and ens and mag:
					items = yield db.select("SELECT * FROM objectifs where an = "+an+" and idens = "+str(ens)+" and idmag = "+str(mag))
				else:	
					items = yield db.select("SELECT * FROM objectifs")	
				#SERIALISATION DU DECIMAL
				for it in items:
					it['ca'] =float(it['ca'])
					it['pm'] =float(it['pm'])
					it['dtm']=str(it['dtm'])
					it['dt']=str(it['dt'])
				
				# Afficher 52 lignes
				dataTable = []
				for i in range(1,53):
					j=0
					for item in items: 
						if item['ids'] == i:
							dataTable.append(item)
							j=1
							break

					if j == 0:
						dataDict = {}
						dataDict['ids'] = i
						dataDict['ca'] = ''
						dataDict['pm'] = ''
						dataDict['ncli'] = ''
						dataDict['comment'] = ''
						dataDict['vise'] = ''
						dataDict['dt'] = ''
						dataDict['dtm'] = ''
						dataDict['usr'] = ''
						dataTable.append(dataDict)

				self.write(json.dumps(dataTable))

		except:
			traceback.print_exc()

def make_app():
	return tornado.web.Application([
		(r"/(.*)", ObjectifsHandler),
	],
	debug=True,
	static_path=os.path.join(ROOT_PATH, 'web'),
	template_path=os.path.join(ROOT_PATH, 'templates')
	)

if __name__ == "__main__":
	app = make_app()
	app.listen(options.http_port)
	print("Listening on port %s" % options.http_port)
	tornado.ioloop.IOLoop.current().start()
