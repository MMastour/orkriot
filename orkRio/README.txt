+---------------------------+
| Objectifs using flotchart |
+---------------------------+

Exemple d'utilisation de tornado avec des coroutines et de requêtes SQL (insert, delete, update, select).

# Arborescence

├── bin       : script utiles
├── database  : schema SQL de la bdd
├── orkit     : kit Orika avec librairie de connexion à la bdd
└── templates : couche de présentation (template index.html, *.tag)
└── web       : couche de présentation (Statique: css, js, images,node_modules)

node_modules:Contient les dépendances de RIOT JS
 
 
 #Accés à l'application :
 Nom de fichier de configuration de serveur tornado : server.py (la version python2.7 est requise )
 Adresse  : localhost
 Port     : 10000
 
 #exemple d'utilisation
 
 étape 1 : démarrer le serveur tornado
 python server.py  

 étape 2 : ouvrir un navigateur web et accéder à l'adresse suivante
 http://0.0.0.0:10000
 
 Tips:
 	Pour effectuer la rechercher:
 		-Commencez par remplir le champ pour lancer l'autocomplete
 		-Clicquez sur la valeur souhaitée
 		-Effectuez la même opération pour les trois champs et cliquez sur le bouton recherche
 
 
