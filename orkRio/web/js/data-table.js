riot.tag2('data-table', '<table id="mytable" class="ui celled table"> <thead> <tr id="tableHeader"> <th>Semaine <div><i id="ids" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="ids" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon {triverif:asc}"></i></div> </th> <th>CA <div><i id="ca" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="ca" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Panier Moyen <div><i id="pm" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="pm" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Nb Client <div><i id="ncli" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="ncli" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Commentaire <div><i id="comment" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="comment" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Visé <div><i id="vise" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="vise" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Date Création <div><i id="dt" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="dt" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Date Modification <div><i id="dtm" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="dtm" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> <th>Utilisateur <div><i id="usr" onclick="{takeIdTri(\'desc\')}" class="sort Attributes descending icon"></i> <i id="usr" onclick="{takeIdTri(\'asc\')}" class="sort Attributes ascending icon"></i></div> </th> </tr> </thead> <tbody> <tr each="{opts.items}"> <form name="formulaire"> <input type="hidden" id="idobj" name="idobj" value="{idobj}"> <td id="ids">{ids}</td> <td id="ca">{ca}</td> <td id="pm">{pm}</td> <td id="ncli">{ncli}</td> <td id="comment">{comment}</td> <td>{vise}</td> <td id="dt">{dt}</td> <td id="dtm">{dtm}</td> <td id="usr">{usr}</td> </form> </tr> </tbody> </table>', '', '', function(opts) {


this.takeIdTri = function(verifTri) {
  return function(e) {
    if (verifTri == 'asc')
        order = 'ASC';
    else
        order = 'DESC';
        myId = e.target.id;
      var data = {

            critere: myId,
            order : order
        };
        var dataToSend = JSON.stringify(data);
        $.ajax({
                url: '/tri'
                , type: 'POST'
                ,data : dataToSend
                , dataType : 'json'
                , success: function (jsonResponse) {
                    var items = jsonResponse;

                riot.mount('data-table', {items: items });

                    }

                , error: function () {
                    $("#tbody").text("Error to load api");
                }
            });
      }
  }.bind(this)

});