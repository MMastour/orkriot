riot.tag2('flot-chart', '<div class="demo-container"> <div id="placeholder" class="demo-placeholder"></div> </div>', '', '', function(opts) {
	this.on('mount', function(){

		$.plot(("#placeholder"), opts.data, {

                    lines: {
              show: true,
                        fillColor: {
                            colors: [{ opacity: 1 }, { opacity: 1 } ]}},
            points: {
              show: true,
                        fillColor: '#bc6bff'},
              grid: {
                        hoverable: true,
                        clickable: true}
                });

		});
});