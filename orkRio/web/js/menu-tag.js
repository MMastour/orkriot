riot.tag2('menu-tag', '<div class="ui pointing menu"> <a class="item" onclick="{lancerGraphe}"> GRAPHE </a> <div class="right menu"> <div class="autoComplete"> <div class="item"> <div class="ui transparent icon input"> <input id="searchAn" placeholder="Année..." type="text" onkeyup="{dropSearch(\'an\')}"></<input> </div> </div> <div id="dropAn" class="drop-menu" hidden="hidden"> <ul> <li each="{an, item in items}" onclick="{takeValue(an[\'an\'],\'an\')}"> {an[\'an\']} </li> </ul> </div> </div> <div class="autoComplete"> <div class="item"> <div class="ui transparent icon input"> <input id="searchEns" placeholder="Enseigne..." type="text" onkeyup="{dropSearch(\'ens\')}"></<input> </div> </div> <div id="dropEns" class="drop-menu" hidden="hidden"> <ul> <li each="{ens, item in items}" value="{ens[\'idens\']}" onclick="{takeValue(ens[\'nom\'],\'ens\')}"> {ens[\'nom\']} </li> </ul> </div> </div> <div class="autoComplete"> <div class="item"> <div class="ui transparent icon input"> <input id="searchMag" placeholder="Magasin..." type="text" onkeyup="{dropSearch(\'mag\')}"></<input> <i class="search link icon" onclick="{rechercher}"></i> </div> </div> <div id="dropMag" class="drop-menu" hidden="hidden"> <ul> <li each="{mag, item in items}" value="{mag[\'idmag\']}" onclick="{takeValue(mag[\'nom\'],\'mag\')}"> {mag[\'nom\']} </li> </ul> </div> </div> </div> </div>', '', '', function(opts) {


var idEns = 0;
var idMag = 0;
this.lancerGraphe = function (e) {

        $.ajax({
            url: '/graphe'
            , type: 'GET'
            , dataType: 'json'

            , success: function (jsonResponse) {

                var objresponse = jsonResponse;
                var data = [{data: objresponse, color: "#4c1876",label: "Chiffre d'affaire par semaine"}];

                riot.mount('flot-chart',{data : data});

            }
        });
},

this.dropSearch = function(condition) {
  return function(e) {
      var valeur = e.target.value;

      var data = {critere : valeur,
                  condition : condition,
                  idEns : idEns
      };
        var dataToSend = JSON.stringify(data);
    $.ajax({
            url: '/datacomplete'
            ,type: 'POST'
            ,data: dataToSend

            , success: function (jsonResponse) {

                items = JSON.parse(jsonResponse);

                if (condition == 'an')
                {
                      if((items.length == 0 || !valeur) && condition == 'an')
                        {$("#dropAn").fadeOut(300); }
                      else
                        {$("#dropAn").fadeIn(300); }
                }

                else if (condition == 'ens')
                {
                      if((items.length == 0 || !valeur) && condition == 'ens')
                        {$("#dropEns").fadeOut(300);}
                      else
                        {$("#dropEns").fadeIn(300);}
                }

                else
                {
                      if((items.length == 0 || !valeur) && condition == 'mag')
                        {$("#dropMag").fadeOut(300);}
                      else
                        {$("#dropMag").fadeIn(300);}
                }

            }
        });
  }
}.bind(this)

this.takeValue = function(valeurChoisie,condition) {
  return function(e) {

    if(condition == 'an')
    {
      this.searchAn.value = valeurChoisie;
        $("#dropAn").fadeOut(300);
    }

    else if(condition == 'ens')
    {
      this.searchEns.value = valeurChoisie;
      idEns = e.target.value;
        $("#dropEns").fadeOut(300);
    }

    else
    {
      this.searchMag.value = valeurChoisie;
      idMag = e.target.value;
      console.log(idMag);
        $("#dropMag").fadeOut(300);
    }

    }
  }.bind(this)

  this.rechercher = function (e){

    inputValueAn = this.searchAn.value;

     var data = {

            an: inputValueAn,
            ens: idEns,
            mag: idMag
        };
        console.log(data);
        var dataToSend = JSON.stringify(data);
        $.ajax({
                url: '/recherche'
                , type: 'POST'
                ,data : dataToSend
                , dataType : 'json'
                , success: function (jsonResponse) {
                    var items = jsonResponse;
                riot.mount('data-table', {items: items });

                    }

                , error: function () {
                    $("#tbody").text("Error to load api");
                }
            });

  }
});