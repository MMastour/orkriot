# coding: utf-8

import logging
import tornado.ioloop
import tornado.gen
import time
import socket

#Non-blocking UDP in Tornado web's IOLoop
# From http://kyle.graehl.org/coding/2012/12/07/tornado-udpstream.html

class UDPStream(object):
    def __init__(self, socket, _timeout=1.0, _sizebuffer=4096, in_ioloop=None):
        self.socket = socket
        self._state = None
        self._read_callback = None
        self._timeout = _timeout
        self._sizebuffer = _sizebuffer
        self.ioloop = in_ioloop or tornado.ioloop.IOLoop.instance()


    def _connect(self, address, callback=None):
        try:
            self.socket.connect(address)
            if callback:
                callback()
        except socket.error as err:
            logging.error("connect error : %s", err)

    def _add_io_state(self, state):
        if self._state is None:
            self._state = tornado.ioloop.IOLoop.ERROR | state
            self.ioloop.add_handler(
                self.socket.fileno(), self._handle_events, self._state)
        elif not self._state & state:
            self._state = self._state | state
            self.ioloop.update_handler(self.socket.fileno(), self._state)

    def send(self,msg):
        try:
            return self.socket.send(msg)
        except:
            return False

    def sendto(self,msg, address):
        try:
            return self.socket.sendto(msg, address)
        except:
            return False

    def recv(self,sz):
        return self.socket.recv(sz)

    def close(self):
        if self.socket:
            self.ioloop.remove_handler(self.socket.fileno())
            self.socket.close()
        self.socket = None

    def read_chunk(self, _callback=None, callback=None):
        callback = _callback if _callback else callback
        self._read_callback = callback
        self._read_timeout = self.ioloop.add_timeout(time.time() + self._timeout,
            self.check_read_callback )
        self._add_io_state(self.ioloop.READ)

    def check_read_callback(self):
        if self._read_callback:
            # XXX close socket?
            self._read_callback(data=None, error='timeout')

    def _handle_read(self):
        if self._read_timeout:
            self.ioloop.remove_timeout(self._read_timeout)
        if self._read_callback:
            try:
                data = self.socket.recv(self._sizebuffer)
            except:
                # conn refused??
                data = None
            self._read_callback(data=data)
            self._read_callback = None

    def _handle_events(self, fd, events):
        if events & self.ioloop.READ:
            self._handle_read()
        if events & self.ioloop.ERROR:
            time.sleep(0.2)     # Avoid high CPU usage when an error occurred, ex. when tpv is down
            # logging.error('%s event error' % self)


############################################################
##############################TEST##########################
############################################################
@tornado.gen.coroutine
def on_response(data, error=None):
    print ("on_response::<<%s>> %s" % (data, (", error:%s" % error) if error else ""))

@tornado.gen.coroutine
def main():
    udpsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udpsock.setblocking(False)
    try:
        udpsock.connect( ('192.168.100.16', 3363 ))
    except socket.error as err:
        return
    s = UDPStream(udpsock)
    s.send( 'some data' )
    #yield tornado.gen.Task( func=s.read_chunk, _callback=on_response )
    args, kwargs = yield tornado.gen.Task( func=s.read_chunk)
    print "<<%s>>" % (str(kwargs))
    print "ok"

if __name__ == '__main__':
    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.run_sync(main)
