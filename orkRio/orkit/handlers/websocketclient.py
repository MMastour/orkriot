# coding: utf-8
import logging
from orkit.utils import json_encode, json_decode
from tornado import httpclient
from tornado import httputil
from tornado import ioloop
from tornado import websocket

DEFAULT_CONNECT_TIMEOUT = 10
DEFAULT_REQUEST_TIMEOUT = 10
APPLICATION_JSON = 'application/json'


class WebSocketClient(object):
	"""
		Generic class to handle a WebSocket client-side
	"""
	def __init__(self, host, port, url,
			 connect_timeout=DEFAULT_CONNECT_TIMEOUT, request_timeout=DEFAULT_REQUEST_TIMEOUT):
		self.connected = False
		self.host = host
		self.port = port
		self.url = url
		self.connect_timeout = connect_timeout
		self.request_timeout = request_timeout
		self.connection()

	def connection(self):
		"""Connect to the server.
        """
		url = "ws://%s:%s/%s" % (self.host, self.port, self.url)
		logging.debug("Connection : url::%s", url)

		headers = httputil.HTTPHeaders({'Content-Type': APPLICATION_JSON})
		request = httpclient.HTTPRequest(url=url,
										 connect_timeout=self.connect_timeout,
										 request_timeout=self.request_timeout,
										 headers=headers)
		self.ws = websocket.WebSocketClientConnection(ioloop.IOLoop.current(),
													  request=request, on_message_callback=self.on_message)
		self.ws.connect_future.add_done_callback(self._on_connect)

	# def connection(self):
	# 	websocket.websocket_connect(url="ws://%s:%s/%s" %(self.host, self.port, self.url),connect_timeout=self.CONNECT_TIMEOUT,callback=self.on_connect,on_message_callback=self.on_message)

	def _on_str_message(self, data):
		"""This is called when new message is available from the server.
		:param data : string message
		"""
		pass

	def _on_message(self, **data):
		"""This is called when new message is available from the server.
		:param data : dictionnary
		"""
		pass

	def on_message(self,message):
		logging.debug("WebSocketClient on_message %s, %s", message, self.host)
		try:
			if message is None:
				self._on_connection_close()
			elif len(message) > 0:
				msg = json_decode(message)
				if type(msg) is dict:
					self._on_message(**msg)
				else :
					self._on_str_message(msg)
		except Exception,ex:
			logging.error("Exception on_message_callback %s message : %s", str(ex), message)

	def _on_connect(self, future):
		if future.exception() is None:
			self.connected = True
			self.ws = future.result()
			self._on_connection_success()
		else:
			self.connected = False
			self._on_connection_error(future.exception())


	def _on_connection_success(self):
		"""This is called on successful connection ot the server.
		"""
		pass

	def _on_connection_error(self, exception):
		"""This is called in case if connection to the server could
		not established.
		"""
		pass

	def _on_connection_close(self):
		"""This is called when server closed the connection.
		"""
		self.connected = False

	def close(self):
		"""close the socket connection
		"""
		if self.connected:
			self.ws.close()
			self.connected = False

	def sendRawData(self, data):
		""" Send raw data with no encoding
		:param self:
		:param data: message to send
		"""
		if self.connected:
			self.ws.write_message(data)

	def send(self, data):
		"""
		Send data with JSON encoding
		:param data: message to send
		:return:
		"""
		self.sendRawData(json_encode(data))

class WebSocketClientAutoReconnection(WebSocketClient):
	"""
		Generic class to handle a WebSocket client-side with an automatic reconnection if connection is closed
	"""
	CONNECT_TIME = 5000			#Temps entre chaque tentative de reconnexion (ms)

	def __init__(self, host, port, url):
		self.reconnect_auto = ioloop.PeriodicCallback(self.connection, self.CONNECT_TIME)
		WebSocketClient.__init__(self, host, port, url)

	def connection(self):
		#Stop la reconnexion automatique en attendant la réponse
		if self.reconnect_auto.is_running():
			self.reconnect_auto.stop()
		WebSocketClient.connection(self)

	def _on_connection_success(self):
		WebSocketClient._on_connection_success(self)
		if self.reconnect_auto.is_running():
			self.reconnect_auto.stop()

	def _on_connection_error(self, exception):
		WebSocketClient._on_connection_error(self, exception)
		# Active la reconnexion automatique
		if not self.reconnect_auto.is_running():
			self.reconnect_auto.start()

	def _on_connection_close(self):
		WebSocketClient._on_connection_close(self)
		# Active la reconnexion automatique
		if not self.reconnect_auto.is_running():
			self.reconnect_auto.start()


class WebSocketClientAutoResendMsg(WebSocketClientAutoReconnection):
	"""
		Generic class to handle a WebSocket client-side
		with an automatic reconnection if connection is closed
		and resending messages which cannot be sent
	"""

	def __init__(self, host, port, url):
		self.listmsg = []			#Liste des messages non envoyés en cas de déconnexion
		WebSocketClientAutoReconnection.__init__(self, host, port, url)

	def _on_connection_success(self):
		WebSocketClientAutoReconnection._on_connection_success(self)
		#Reprend la liste des messages non envoyés
		l = self.listmsg
		self.listmsg = []
		for data in l:
			self.sendRawData(data)

	def send(self, data):
		if self.connected :
			self.ws.write_message(json_encode(data))
		else:
			self.listmsg.append(json_encode(data))

	def sendRawData(self, data):
		""" Send raw data with no encoding
		:param self:
		:param data: message to send
		"""
		if self.connected:
			self.ws.write_message(data)
		else:
			self.listmsg.append(data)
