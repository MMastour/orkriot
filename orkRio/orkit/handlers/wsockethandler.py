# coding: utf-8

import logging
import traceback

from tornado import gen
from tornado.escape import json_decode, json_encode
from tornado.websocket import WebSocketHandler

class OrkWebSocketHandler(WebSocketHandler):

    def __init__(self, application, request, **kwargs):
        WebSocketHandler.__init__(self, application, request, **kwargs)
        self.clients = set()

    @gen.coroutine
    def open(self, *args):
        self.clients.add(self)

    @gen.coroutine
    def on_message(self, message):
        logging.debug("OrkWebSocketHandler.on_message :: %s" , message)
        try:
            msg = json_decode(message)
            self._on_message(msg)
        except:
            traceback.print_exc()

    @gen.coroutine
    def _on_message(self, message):
        pass

    @gen.coroutine
    def on_close(self):
        self.clients.remove(self)

    @gen.coroutine
    def send(self, data):
        logging.debug("send data WebSocketHandler ::::: %s", json_encode(data))
        try:
            self.write_message(json_encode(data))
        except:
            traceback.print_exc()

    @gen.coroutine
    def broadcast(self, data):
        logging.debug("%s(OrkWebSocketHandler).broadcast ::: %s, clients :: %s", self.__class__.__name__,  data, self.clients)
        for client in self.clients:
            try:
                client.write_message(json_encode(data))
            except:
                traceback.print_exc()
