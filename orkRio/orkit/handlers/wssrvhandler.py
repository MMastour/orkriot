# coding: utf-8
import json
import logging
import datetime
import traceback

from decimal import Decimal
from orkit.utils.ws.validators import STATUS_ERROR, STATUS_SUCCESS
from orkit.utils import json_decode
from querystring_parser import parser
from tornado import gen, web, escape

#Web Services Handler

class CustomEncoder(json.JSONEncoder):
	"""
	Classe utilitaire pour encoder les dates
	au format datetime.
	"""
	def default(self, obj):
		if isinstance(obj, datetime.datetime) or isinstance(obj, datetime.date) or isinstance(obj, datetime.time) or isinstance(obj, datetime.timedelta):
			return unicode(obj).split("+")[0]
		elif isinstance(obj, Decimal):
			return float(obj)
		else:
			try:
				return json.JSONEncoder.default(self, obj)
			except Exception, ex:
				logging.error("Error JSONEncoder : %s" % ex)
				traceback.print_exc()
				return "Error JSONEncoder : %s" % ex

class BaseHandler(web.RequestHandler):
	@property
	def db(self):
		return self.application.db

	def prepare(self):
		if self.request.method == 'GET':
			data = self.request.query
		else:
			data = self.request.body

		self.json_args = {}
		try:
			if self.request.method == 'GET':
				self.json_args = parser.parse(data)
			else:
				try:
					self.json_args = json_decode(data)
				except:
					self.json_args = parser.parse(data)
		except Exception, ex:
			logging.error("%r >> %s" % (data, ex))

#Main class for common Orika web service API
class MainHandler(BaseHandler):
	@gen.coroutine
	def prepare(self):
		self.set_header("Access-Control-Allow-Origin", "*")

	def _end_post(self, status, errors, data={}):
		if status:
			ret = dict(status=status, errors=errors, data=data)
		else:
			if data:
				ret = dict(status=status, data=data)
			else:
				ret = dict(status=status)
		logging.info("RETURN  ::: %s", ret)
		self.write(ret)
		self.set_header("Content-Type", "application/json; charset=UTF-8")
		self.finish()

	def _get_api_required_keys(self):
		return(
			('user', 'Missing user id'),
			('pwd', 'Missing user password'),
			('action', 'Missing action'),
			('data', 'Missing input data'),
		)

	def _validate_format(self, data_json):
		API_REQUIRED_KEYS = self._get_api_required_keys()
		status, errors = STATUS_SUCCESS, {}
		# API fields control
		for (k, error_msg) in API_REQUIRED_KEYS :
			if not data_json.has_key(k):
				status = STATUS_ERROR
				errors['__GLOBAL__'] = errors.get('__GLOBAL__') and ', '.join((errors.get('__GLOBAL__'), error_msg)) or error_msg
		for k in data_json.keys():
			ACCEPTED_KEYS = dict(API_REQUIRED_KEYS).keys()
			ACCEPTED_KEYS.append('version')
			if k not in ACCEPTED_KEYS:
				status = STATUS_ERROR
				error_msg = 'Unknown Field : %s' % k
				errors['__GLOBAL__'] = errors.get('__GLOBAL__') and ', '.join((errors.get('__GLOBAL__'), error_msg)) or error_msg
		return status, errors

	def _auth_user(self, data_json):
		return STATUS_SUCCESS, {}

	def _auth_action(self, data_json):
		return STATUS_SUCCESS, {}

	@gen.coroutine
	def execute_action(self, action, input_data, *args, **kwargs):
		raise NotImplementedError('subclasses must override execute_action()!')

	@gen.coroutine
	def post(self, *args, **kwargs):
		#logging.error(self.request.body)
		try:
			data_json = escape.json_decode(self.request.body)
			access_validators = [
				self._validate_format,
				self._auth_user,
				self._auth_action
			]
			for validator in access_validators:
				status, errors = validator(data_json)
				if status:
					self._end_post(status, errors)
					raise gen.Return()
			# all checks ok, let's process action
			input_data, action = data_json.get('data'), data_json.get('action')
			logging.info("Received request :::: %s, %s", action, input_data)
			status, errors, data = yield self.execute_action(action, input_data, *args, **kwargs)
		except ValueError, ex:
			status = STATUS_ERROR
			errors = {'__GLOBAL__': ex }
			logging.error("Value error : %s", ex)
			data = None

		self._end_post(status, errors, data)
