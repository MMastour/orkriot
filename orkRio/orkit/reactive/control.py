# coding: utf-8

import logging
from tornado import gen

class Control:
	"""
	Manage the actions and the triggers for all stores and components
	"""

	__stores = []
	__components = []

	@staticmethod
	def addStore(store):
		"""
		Add a store to the controller
		:param store:
		"""
		Control.__stores.append(store)

	@staticmethod
	def addComponent(component):
		"""
		Add a component to the controller
		:param component:
		"""
		Control.__components.append(component)

	@staticmethod
	def off(action, callback):
		"""
		Remove callback function from the actions of all stores and compnents
		:param action: Action name
		:param callback:
		"""
		for store in Control.__stores:
			store.off(action, callback)
		for component in Control.__components:
			component.off(action, callback)

	@staticmethod
	def on(action, callback):
		"""
		Add callback function to the actions in all stores and compnents
		:param action: Action name
		:param callback:
		"""
		for store in Control.__stores:
			store.on(action, callback)
		for component in Control.__components:
			component.on(action, callback)

	@staticmethod
	def one(action, callback):
		"""
		Add callback function to the actions in all stores and compnents
		:param action: Action name
		:param callback:
		"""
		for store in Control.__stores:
			store.one(action, callback)
		for component in Control.__components:
			component.one(action, callback)

	@staticmethod
	def reset():
		"""
		Initiliaze stores and components
		"""
		Control.__stores = []
		Control.__components = []

	@staticmethod
	def stores():
		"""
		:return: All stores
		"""
		return Control.__stores

	@staticmethod
	def components():
		"""
		:return: All components
		"""
		return Control.__components

	@staticmethod
	@gen.coroutine
	def trigger(action, **data):
		"""
		Trriger the action in all stores and components
		:param action: Action name
		:param data: Arguments
		"""
		logging.debug("Trigger %s", action)
		for store in Control.__stores:
			store.trigger(action, **data)
		for component in Control.__components:
			component.trigger(action, **data)
