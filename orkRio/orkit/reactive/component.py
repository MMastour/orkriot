# coding: utf-8

from orkit.reactive.reactitem import ReactItem

class Component(ReactItem):
	"""
	Reactive component
	"""

	def status(self):
		raise NotImplementedError('subclasses must override status()!')
