# coding: utf-8

import logging
from tornado import gen

class ReactItem:
	"""
	Reactive element.
	Recording all actions and the function called expected by the element.
	"""

	def __init__(self):
		self.__actions = {}
		self.__onceActions = {}

	@gen.coroutine
	def on(self, action, callback):
		"""
		Add callback function to the action
		:param action: Action name
		:param callback: Function called
		"""
		callbacks = self.__actions.get(action, [])
		callbacks.append(callback)
		self.__actions[action] = callbacks

	@gen.coroutine
	def one(self, action, callback):
		"""
		Add callback function to the action
		:param action: Action name
		:param callback: Function called
		"""
		callbacks = self.__onceActions.get(action, [])
		callbacks.append(callback)
		self.__onceActions[action] = callbacks

	@gen.coroutine
	def off(self, action, callback=None):
		"""
		Remove callback function from the actions
		:param action: Action name
		:param callback:
		"""
		if action in self.__actions:
			if callback:
				callbacks = self.__actions[action]
				self.__actions[action] = filter(lambda cb: cb != callback, callbacks)
			else:
				del self.__actions[action]

		if action in self.__onceActions:
			if callback:
				callbacks = self.__onceActions[action]
				self.__onceActions[action] = filter(lambda cb: cb != callback, callbacks)
			else:
				del self.__onceActions[action]

	@gen.coroutine
	def trigger(self, action, **data):
		"""
		Call all the callback functions bound with the action
		:param action: Action name
		:param data: Arguments of the triggers
		:return:
		"""
		internalAction = 'on_%s' % action
		if hasattr(self, internalAction):
			getattr(self, internalAction)(**data)

		callbacks = self.__actions.get(action, [])
		for callback in callbacks:
			callback(**data)

		callbacks = self.__onceActions.get(action, [])
		if callbacks:
			del self.__onceActions[action]
			for callback in callbacks:
				callback(**data)

	def actions(self):
		return self.__actions

	def onceactions(self):
		return self.__onceActions

	def internalactions(self):
		internalaction = {}
		for (prop, callback) in self.__class__.__dict__.items():
			if prop.startswith('on_'):
				action = prop[len('on_'):]
				if not internalaction.get(action):
					internalaction[action] = []
				internalaction[action].append(getattr(self, prop))
		return internalaction

	def __repr__(self):
		return self.__class__.__name__

