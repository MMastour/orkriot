# coding: utf-8
import sys
import time
import logging
import tornado.gen

"""
Orkit : Decorators
"""

reload(sys)
sys.setdefaultencoding('utf-8')

def timestamp(func):
    """
    A Python decorator for adding an arguments with timestamp in function
    """
    def wrapper(*args, **kwargs):
        kwargs['tstamp'] = time.time()
        func(*args, **kwargs)

    return wrapper


def benchmark(func):
    """
    A Python decorator for measuring the execution time of methods
    """
    import time
    def wrapper(*args, **kwargs):
        t = time.clock()
        res = func(*args, **kwargs)
        logging.info("BENCHMARK ...... : %s %s", func.__name__, time.clock()-t)
        return res
    return wrapper


def benchmarkTornado(func):
    """
    A Python decorator for measuring the execution time of methods
    """
    import time
    @tornado.gen.coroutine
    def wrapper(*args, **kwargs):
        t = time.clock()
        res = yield func(*args, **kwargs)
        logging.info("BENCHMARK ...... : %s %s", func.__name__, time.clock()-t)
        raise tornado.gen.Return(res)
    return wrapper
