# coding: utf-8

from decimal import Decimal
from datetime import date
from datetime import datetime
from datetime import time
from datetime import timedelta
import simplejson as json

def _jdefault(o):
	if isinstance(o, set):
		return list(o)
	elif isinstance(o, datetime):
		return str(o)
	elif isinstance(o, timedelta):
		return str(o)
	elif isinstance(o, date):
		return str(o)
	elif isinstance(o, time):
		return str(o)
	elif isinstance(o, Decimal):
		return str(o)
	elif hasattr(o, '__class__'):
		return o.__dict__
	return o

def json_encode(o, compact=True):
	if compact:
		return json.dumps(o, default=_jdefault, separators=(',', ':'))
	return json.dumps(o, default=_jdefault)

def json_decode(o):
	return json.loads(o, use_decimal=True, strict=False)

