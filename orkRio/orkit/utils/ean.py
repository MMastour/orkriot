# coding: utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

def genereEan13(eanbase):
    """Génère une ean 13 valide si l'ean donné en entré fait moins de 13 chiffres, si l'ean à au moins 13 numérique le renvoi tel quel, en cas d'erreur retourne None."""
    try:
        int(eanbase)
    except:
        return None

    ean = str(eanbase)
    if len(ean)>=13:
        return eanbase
    else:
        ean = ean.zfill(12)
    # UPCA/EAN13
    weight=[1,3]*6
    magic=10
    sum = 0

    for i in range(12): # checksum based on first 12 digits.
        sum = sum + int(ean[i]) * weight[i]
    z = ( magic - (sum % magic) ) % magic
    return ean + str(z)



def genereDigit(code):
    """Génère un digit"""
    try:
        int(code)
    except:
        return None

    code = str(code)

    d = 1111 - sum(map(int, list(code)))
    while d > 10:
        d = d % 10
    return code + str(d)

if __name__ == '__main__':
    #print(genereEan13('903200506289'))
    print(genereDigit('99920160202001345678001'))

