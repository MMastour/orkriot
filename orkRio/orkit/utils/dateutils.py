# -*- coding: utf-8 -*-

import datetime
import time

def aujourdhui():
	"""Renvoie la date du jour au format français jj-mm-aaaa.
	"""
	return datetime.datetime.today().strftime('%d-%m-%Y')

def deltadays(dt1, dt2):
	"""Renvoie le nombre de jours entre 2 dates (dt2-dt1+1).

	>>> deltadays(dt1='2011-01-01', dt2='2011-01-01')
	1
	>>> deltadays(dt1='2011-01-01', dt2='2011-01-03')
	3
	"""
	d1 = datetime.date(*tuple(map(int, dt1.split('-'))))
	d2 = datetime.date(*tuple(map(int, dt2.split('-'))))
	return int((d2-d1).days + 1)

def digitify(a):
	"""Renvoie les chiffres de la chaîne données.
	"""
	if a is None:
		return ''
	return "".join(filter(lambda c: c.isdigit(), a))

def dow(dt):
	"""Renvoie le numéro du jour dans la semaine de la date donnée au format américain aaaa-mm-dd (1: lundi - 7: dimanche).
	
	>>> dow('2011-04-17') # dimanche
	7
	>>> dow('2011-04-11') # lundi
	1
	"""
	an, mois, jour = map(int, dt.split('-'))
	y, w, d = datetime.date(an, mois, jour).isocalendar()
	return d

def doy(dt):
	"""Renvoie le numéro du jour dans l'année de la date donnée au format américain aaaa-mm-dd (1: pour le 01/01).
	
	>>> doy('2011-01-01')
	1
	>>> doy('2011-02-01')
	32
	"""
	an, mois, jour = map(int, dt.split('-'))
	d = datetime.date(an, mois, jour).timetuple().tm_yday
	return d

def dt2wy(dt):
	"""Renvoie le numero de semaine et l'année de cette semaine pour une date donnée (format américain aaaa-mm-jj).
	"""
	an, mois, jour = map(int, dt.split('-'))
	an, sem, _xxx = datetime.date(an, mois, jour).isocalendar()
	return sem, an

def dtfr(dt):
	"""Renvoie la date au format français jj-mm-aaaa.
	"""
	try:
		aaaa, mm, jj = map(int, dt.split()[0].replace('/','-').split('-'))
		if aaaa<=31:
			jj, aaaa = aaaa, jj
		return "%02d-%02d-%04d" % (jj, mm, aaaa)
	except:
		return dt 

def dtisfr(dt):
	"""Renvoie un booléen indiquant si la date est au format français jj-mm-aaaa.
	"""
	try:
		jj, mm, aaaa = map(int, dt.split()[0].replace('/','-').split('-'))
		time.strptime("%02d-%02d-%04d" % (jj,mm,aaaa), "%d-%m-%Y")
		return True
	except:
		return False

def dtisus(dt):
	"""Renvoie un booléen indiquant si la date est au format américain aaaa-mm-jj.
	"""
	try:
		aaaa, mm, jj = map(int, dt.split()[0].replace('/','-').split('-'))
		time.strptime("%04d-%02d-%02d" % (aaaa,mm,jj), "%Y-%m-%d")
		return True
	except:
		return False

def dtus(dt):
	"""Renvoie la date au format américain aaaa-mm-dd.
	"""
	try:
		jj, mm, aaaa = map(int, dt.split()[0].replace('/','-').split('-'))
		if aaaa<=31:
			aaaa, jj = jj, aaaa
		return "%04d-%02d-%02d" % (aaaa, mm, jj)
	except:
		return dt 

def duree(start, end):
        """Renvoie la durée entre deux temps en millisecondes sous une forme de caractères lisible pour un humain.
	"""
        delta = end-start
        if delta<60:
                return "%ds" % delta
        if delta<3600:
                return "%dm%ds" % ( delta/60, delta%60 )
        return "%dh%dm%ds" % ( delta/3600, (delta%3600)/60, delta%60)

def firstdom(dt):
	"""Renvoie la date du premier jour du mois de la date donnée au format américain aaaa-mm-dd.
	"""
	an, mois, jour = map(int, dt.split('-'))
	return '%d-%s-01' % (an, str(mois).zfill(2))

def firstdow(dt):
	"""Renvoie la date du lundi de la semaine de la date donnée au format américain aaaa-mm-dd.
	"""
	an, mois, jour = map(int, dt.split('-'))
	y, w, d = datetime.date(an, mois, jour).isocalendar()
	fourth_jan = datetime.date(y, 1, 4)
	delta = datetime.timedelta(fourth_jan.isoweekday()-1)
	year_start = fourth_jan - delta
	return str(year_start + datetime.timedelta(0, 0, 0, 0, 0, 0, w-1))

def firstdoy(dt):
	"""Renvoie la date du premier jour de l'année de la date donnée au format américain aaaa-mm-dd.
	"""
	an = int(dt.split('-')[0])
	return '%d-01-01' % an

def lastdom(dt):
	"""Renvoie la date du dernier jour du mois de la date donnée au format américain aaaa-mm-dd.
	"""
	an, mois, jour = map(int, dt.split('-'))
	d = datetime.date(an, mois, jour)
	if d.month == 12:
		return str(d.replace(day=31))
	return str(d.replace(month=d.month+1, day=1) - datetime.timedelta(days=1))

def lastdow(dt):
	"""Renvoie la date du dimanche de la semaine de la date donnée au format américain aaaa-mm-dd (1: lundi - 7: dimanche).
	"""
	an, mois, jour = map(int, dt.split('-'))
	y, w, d = datetime.date(an, mois, jour).isocalendar()
	fourth_jan = datetime.date(y, 1, 4)
	delta = datetime.timedelta(fourth_jan.isoweekday()-1)
	year_start = fourth_jan - delta
	return str(year_start + datetime.timedelta(6, 0, 0, 0, 0, 0, w-1))

def lastdoy(dt):
	"""Renvoie la date du dernier jour de l'année de la date donnée au format américain aaaa-mm-dd.
	"""
	an = int(dt.split('-')[0])
	return '%d-12-31' % an

def maintenant():
	"""Renvoie la date i et l'heure du jour au format français jj-mm-aaaa HH:MM:SS.
	"""
	return datetime.datetime.today().strftime('%d-%m-%Y %H:%M:%S')

def newday(dt, n):
	"""Renvoie la date donnée décalée de +/- n jours (format américain aaaa-mm-jj).
	"""
	an, mois, jour = map(int, dt.split('-'))
	d = datetime.date(an, mois, jour)
	return str(d + datetime.timedelta(n))

def now():
	"""Renvoie la date i et l'heure du jour au format américain aaaa-mm-jj HH:MM:SS.
	"""
	return datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')

def today():
	"""Renvoie la date du jour au format américain aaaa-mm-jj.
	"""
	return datetime.datetime.today().strftime('%Y-%m-%d')

def tsfr(dt):
	"""Renvoie la date et l'heure au format français jj-mm-aaaa hh:mm:ss.
	"""
	return "-".join(reversed(dt.split()[0].replace('/','-').split('-'))) + ' ' + dt.split()[1][:5]

def tsus(dt):
	"""Renvoie la date et l'heure au format américain aaaa-mm-jj hh:mm:ss.
	"""
	return "-".join(reversed(dt.split()[0].replace('/','-').split('-'))) + ' ' + dt.split()[1][:5]

def wy2dt(w,y):
	"""Renvoie la date du lundi (format américain aaaa-mm-dd) de la semaine et de l'année données.
	"""

	fourth_jan = datetime.date(y, 1, 4)
	delta = datetime.timedelta(fourth_jan.isoweekday()-1)
	year_start = fourth_jan - delta
	return str(year_start + datetime.timedelta(0, 0, 0, 0, 0, 0, w-1))

def timestr():
	"""Renvoie l'heure local actuelle au format HH:MM:SS.
	"""
	return time.strftime('%H:%M:%S', time.localtime())