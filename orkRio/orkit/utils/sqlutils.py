# -*- coding: utf-8 -*-

"""Outils pour générer du code sql."""

import re
import time
import sets
import types

import dateutils as du_
import stringutils as su_

def a2sql(s, strips=True):
	"""
	Formate une valeur python en SQL sans mettre des quotes pour les nombres. Par exemple None sera transformée en 'NULL'

	@param s: la chaîne de caractères à traiter
	@param strips: option pour supprimer ou non les blancs à gauche et à droite
	@type s: string
	@rtype: string
	@return: la chaîne de caractères SQL
	"""

	return sqlize(s=s, strips=strips, quoteNumber=False)

def between(name,fromValue,toValue,nullable=True):
	"""
	Renvoie une chaîne SQL permettant de tester si la colonne name se trouve entre deux dates
	fromValue et toValue
	
	@param name: la colonne SQL à tester
	@type name: string
	@param fromValue: la date inférieure au format 'yyyy-mm-dd'
	@type fromValue: string
	@param toValue: la date supérieure au format 'yyyy-mm-dd'
	@type toValue: string
	@rtype: string
	@return: une chaîne SQL
	@warning: attribut nullable non gere, rajouter le cas de verif que la date from est bien inferieure a la date to
	"""
	
	if not su_.isBlank(name) and (du_.isDateFormatted(fromValue,'%Y-%m-%d') or su_.isBlank(fromValue)) and (du_.isDateFormatted(toValue,'%Y-%m-%d') or su_.isBlank(toValue)):
		# date debut et date fin non vides
		if not su_.isBlank(fromValue) and not su_.isBlank(toValue):
			return " (%s between %s and %s) " % (name, sqlize(fromValue), sqlize(toValue))
		# date debut non vide, date fin vide
		if not su_.isBlank(fromValue) and su_.isBlank(toValue):
			return " (%s >= %s) " % (name, sqlize(fromValue))
		# date debut vide, et date fin non vide
		if su_.isBlank(fromValue) and not su_.isBlank(toValue):
			return " (%s <= %s) " % (name, sqlize(toValue))
	return None

def endswith(k,v,nullable=True):
	"""
	Renvoie une chaîne SQL permettant de tester les valeurs de la colonne k se terminent par la valeur v.
	
	@param k: la colonne SQL à tester
	@type k: string
	@param v: la valeur à tester
	@type v: string
	@rtype: string
	@return: une chaîne SQL
	@warning: attribut nullable non géré
	"""

	if (not nullable and v == None) or su_.isBlank(k) or su_.isBlank(v):
		return None
	fmt_v = sqlize('%%%s' % v)
	return " upper(%s::text) LIKE upper(%s::text) " % (k, fmt_v)

def equals(name,value,nullable=True):
	"""
	Renvoie une égalité SQL
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx='yy'
	@warning: attribut nullable non géré
	"""

	if (not nullable and value is None) or su_.isBlank(name) or su_.isBlank(value) :
		return None
	fmt_v = sqlize(value)
	if fmt_v.upper() == "TRUE" or fmt_v.upper() == "FALSE":
		return "%s IS %s" % (name, fmt_v)
	return " upper(%s::text)=upper(%s::text) " % (name, fmt_v)
	
def equalsDate(name,value,nullable=True):
	"""
	Renvoie une égalité SQL pour les dates
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx='yy'
	@warning: attribut nullable non géré
	"""

	if not nullable and value is None or su_.isBlank(name) or su_.isBlank(value) :
		return None
	fmt_v = sqlize(du_.formatDate(value,"%Y-%m-%d"))
	return " %s=%s " % (name, fmt_v)
	
def into(name,values,nullable=True):
	"""
	Renvoie une contrainte dans une liste SQL
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type values: list
	@param value: les valeurs à tester
	@type value: string
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx='yy'
	@warning: attribut nullable non géré
	"""

	if (not nullable and values is None) or su_.isBlank(name) or (type(values)==list and len(values)==0) or (type(values)==str and su_.isBlank(values)) or (type(values) not in [list,str]):
		return None
	if type(values) == str:
		_values = values.split(",")
	else:
		_values = values
	fmt_v = [sqlize(value) for value in _values]
	def __fmt(v):
		if v.upper() in ["TRUE","FALSE"]:
			return v.upper()
		return "upper(%s::text)" % v
	return " upper(%s::text) IN (%s) " % (name, ",".join([__fmt(v) for v in fmt_v]))
	
def extractFilter(lst, params):
	"""
	Extrait et renvoi la liste de contraintes sql à partir du tuple (key,col,func) et des valeurs stockées dans un dictionnaire.
	
	@param lst : la liste des tuples (key,col,func) où key est une clé du dictionnaire, col la/les colonnes sql de recherche, func la fonction à appliquer sur les params pour avoir la contrainte. DANS LE CAS OU func=ilikeMulti, col EST UNE LISTE DE COLONNES
	@param lst: list
	@param params: les données sous forme de dictionnaire.
	@type params: dict
	@rtype: string
	@return: une clause SQL where rassemblant toutes les contraintes de sélection
	"""

	where = []
	if len(lst) != 0 and len(params) != 0:
		for elem in lst:
			if len(elem) != 3:
				continue
			(key,col,func) = elem
			if func == between:
				fromValue = params.get("from_" + key)
				toValue = params.get("to_" + key)
				if not su_.isBlank(fromValue) or not su_.isBlank(toValue):
					where.append( between(col, fromValue, toValue) )
			else:
				value = params.get(key)
				if not su_.isBlank(value) and isinstance(func,types.FunctionType) :
					where.append( func(col, value) )
	return where 

def ilike(name,value,nullable=True):
	"""
	Renvoie une chaîne SQL avec l'opérateur LIKE sans tenir compte de la casse en complétant le critère par % à gauche et à droite.
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une chaîne SQL sous forme de chaîne de type xx LIKE 'yy' ou xx LIKE 'yy'
	@warning: attribut nullable non géré
	"""

	if not nullable and value == None or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize('%%%s%%' % value)
	if fmt_v.upper() == "TRUE" or fmt_v.upper() == "FALSE":
		return "%s IS %s" % (name, fmt_v)
	return " upper(%s::text) LIKE upper(%s::text) " % (name, fmt_v)

def ilikeMulti(names,value,nullable=True):
	"""
	Renvoie une chaîne SQL contenant plusieurs opérateurs LIKE sans tenir compte de la casse en complétant le critère par % à gauche et à droite.
	
	@param names: liste des éléments sur lesquels il faut effectuer un ilike
	@type names: list
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une chaîne SQL sous forme de chaîne de type xx LIKE 'yy' ou xx LIKE 'yy'
	@warning: attribut nullable non géré
	"""

	if (not nullable and value == None) or len(names) == 0 or su_.isBlank(value) or not isinstance(names,types.ListType):
		return None
	fmt_v = sqlize('%%%s%%' % value)
	if fmt_v.upper() == "TRUE" or fmt_v.upper() == "FALSE":
		return "( %s )" % " OR ".join([ "%s IS %s" % (name,fmt_v) for name in names ])
	return "( %s )" % " OR ".join([ "upper(%s::text) LIKE upper(%s::text)" % (name,fmt_v) for name in names ])

def ilikeRaw(name,value,nullable=True):
	"""
	Renvoie une chaîne SQL avec l'opérateur LIKE sans tenir compte de la casse
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une chaîne SQL sous forme de chaîne de type xx LIKE 'yy' ou xx LIKE 'yy'
	@warning: attribut nullable non géré
	"""

	if not nullable and value == None or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize('%s' % value)
	if fmt_v.upper() == "TRUE" or fmt_v.upper() == "FALSE":
		return "%s IS %s" % (name, fmt_v)
	return " upper(%s::text) LIKE upper(%s::text) " % (name, fmt_v)

def ilikeMultiRaw(names,value,nullable=True):
	"""
	Renvoie une chaîne SQL contenant plusieurs opérateurs LIKE sans tenir compte de la casse.
	
	@param names: liste des éléments sur lesquels il faut effectuer un ilike
	@type names: list
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une chaîne SQL sous forme de chaîne de type xx LIKE 'yy' ou xx LIKE 'yy'
	@warning: attribut nullable non géré
	"""

	if (not nullable and value == None) or len(names) == 0 or su_.isBlank(value) or not isinstance(names,types.ListType):
		return None
	fmt_v = sqlize('%s' % value)
	if fmt_v.upper() == "TRUE" or fmt_v.upper() == "FALSE":
		return "( %s )" % " OR ".join([ "%s IS %s" % (name,fmt_v) for name in names ])
	return "( %s )" % " OR ".join([ "upper(%s::text) LIKE upper(%s::text)" % (name,fmt_v) for name in names ])

def greaterThan(name,value,equals=True, nullable=True):
	"""
	Renvoie une chaîne SQL de comparaison spérieure ou égal
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@param equals: paramètre permettant d'indiquer s'il s'agit d'une comparaison stricte (Par défault c'est une comparaison non stricte)
	@type equals: bool
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx>='yy' ou xx>'yy'
	@warning: attribut nullable non géré
	"""
	
	if (not nullable and value is None) or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize(value)
	if equals is True:
		return " %s>=%s " % (name, fmt_v)
	return " %s>%s " % (name, fmt_v)
	
def greaterThanDate(name,value,equals=True,nullable=True):
	"""
	Renvoie une chaîne SQL de comparaison supérieure ou égal sur des dates
	
	@param name: la colonne SQL de type date à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@param equals: paramètre permettant d'indiquer s'il s'agit d'une comparaison stricte (Par défault c'est une comparaison non stricte)
	@type equals: bool
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx>='yy' ou xx>'yy'
	@warning: attribut nullable non géré
	"""

	if not nullable and value is None or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize(du_.formatDate(value,"%Y-%m-%d"))
	if equals is True:
		return " %s>=%s " % (name, fmt_v)
	return " %s>%s " % (name, fmt_v)

def lessThan(name,value,equals=True, nullable=True):
	"""
	Renvoie une chaîne SQL de comparaison inférieur ou égal
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@param equals: paramètre permettant d'indiquer s'il s'agit d'une comparaison stricte (Par défault c'est une comparaison non stricte)
	@type equals: bool
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx<='yy' ou xx<'yy'
	@warning: attribut nullable non géré
	"""
	
	if not nullable and value is None or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize(value)
	if equals is True:
		return " %s<=%s " % (name, fmt_v)
	return " %s<%s " % (name, fmt_v)
	
def lessThanDate(name,value,equals=True,nullable=True):
	"""
	Renvoie une chaîne SQL de comparaison inférieur ou égal sur des dates
	
	@param name: la colonne SQL de type date à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@param equals: paramètre permettant d'indiquer s'il s'agit d'une comparaison stricte (Par défault c'est une comparaison non stricte)
	@type equals: bool
	@rtype: string
	@return: une égalité SQL sous forme de chaîne de type xx<='yy' ou xx<'yy'
	@warning: attribut nullable non géré
	"""
	
	if not nullable and value is None or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize(du_.formatDate(value,"%Y-%m-%d"))
	if equals is True:
		return " %s<=%s " % (name, fmt_v)
	return " %s<%s " % (name, fmt_v)
	
def like(name,value,nullable=True):
	"""
	Renvoie une chaîne SQL avec l'opérateur LIKE
	
	@param name: la colonne SQL à tester
	@type name: string
	@param value: la valeur à tester
	@type value: string
	@rtype: string
	@return: une chaîne SQL sous forme de chaîne de type xx LIKE 'yy' ou xx LIKE 'yy'
	@warning: attribut nullable non géré
	"""

	if not nullable and value == None or su_.isBlank(name) or su_.isBlank(value):
		return None
	fmt_v = sqlize('%s' % value)
	if fmt_v.upper() == "TRUE" or fmt_v.upper() == "FALSE":
		return "%s IS %s" % (name, fmt_v)
	return " %s LIKE %s " % (name, fmt_v)

def now():
	return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

def qdel(table, where=None):
	"""
	Renvoie une requête SQL de type delete. Si where vaut None la table est entièrement effacée
	
	@param table: le nom de la table concernée par la suppression
	@type table: string
	@param where: clause where supplémentaire pour sélectionner les enregsitrements à supprimer
	@type where: string
	@rtype: string
	@return: la requête SQL delete générée	
	"""
	if su_.isBlank(table):
		return None	
	_where = where
	if _where is None or su_.isBlank(_where):
		_where = ''
	else:
		_where = " where " + _where
	return "delete from %s %s;" % (table,_where)

def qdesc(table, con):
	"""
	renvoie la description d'une table SQL cad les colonnes dans 'fields', les colonnes de la clé primaire dans 'keys', les colonnes avec des valeurs par defaut dans 'defaults'

	@param table: le nom de la table
	@type table: string
	@param con: l'objet connexion pg utilisée pour effectuer la requête
	@type con: <objet pg>
	@rtype: dict
	@return: un dictionnaire contenant les clés fields, keys et defaults
	"""

	if su_.isBlank(table) or su_.isBlank(con):
		return None
	res = {}
        # FIELDS
        q="select a.attname as nm from pg_attribute a,  pg_type c where c.typname='%s' and a.attrelid=c.typrelid and a.attnum>0 " % table
	res['fields'] = [ field for [field] in con.query(q).getresult() ]
        # KEYS
        q = "select a.attname as nm from pg_attribute a, pg_index b, pg_type c where c.typname='%s' and b.indrelid=c.typrelid and b.indisprimary and a.attrelid=b.indexrelid" % table
	res['keys'] = [ key for [key] in con.query(q).getresult() ]
        # KEYS WITH DEFAULT VALUE
        q = "select a.attname as nm from pg_attribute a, pg_index b, pg_type c where c.typname='%s' and b.indrelid=c.typrelid and b.indisprimary and a.attrelid=b.indexrelid and a.atthasdef IS TRUE" % table
	res['defaults'] = [ default for [default] in con.query(q).getresult() ]

	return res

def qins(table, data, returning=None, cols=None, rawcols=[]):
	"""
	Renvoie une requête insert faite à partir des arguments
	
	@param table: le nom de la table
	@type table: string
	@param data: le dictionnaire des données à insérer
	@type data: dict
	@param cols: liste des colonnes SQL à prendre en compte dans la requête insert. Par défault les clés de data sont prises en compte
	pour la génération de la requête
	@type cols: list
	@param rawcols: liste des colonnes SQL à prendre sans aucun traitement particulier
	@type rawcols: list
	@rtype: string
	@return: la requête SQL insert générée
	"""
	return qins82(table=table, data=data, returning=returning, cols=cols, rawcols=rawcols)

def qins82(table, data, returning, cols=None, rawcols=[]):
	"""
	Renvoie une requête insert faite à partir des arguments (si returning est différent de None, il faut postgresql >= 8.2)
	
	@param table: le nom de la table
	@type table: string
	@param data: le dictionnaire des données à insérer
	@type data: dict
	@param cols: liste des colonnes SQL à prendre en compte dans la requête insert. Par défault les clés de data sont prises en compte
	pour la génération de la requête
	@type cols: list
	@param rawcols: liste des colonnes SQL à prendre sans aucun traitement particulier
	@type rawcols: list
	@param returning: valeur du champ à renvoyer après l'insert réussit
	@type returning: string
	@rtype: string
	@return: la requête SQL insert générée avec un returning si demandé
	"""
	
	if su_.isBlank(table) or len(data) == 0 or not isinstance(data,types.DictType):
		return None	

	_cols = cols
	if _cols is None:
		_cols = data.keys()

	_returning = ""
	if returning is not None:
		if type(returning) == list:
			_returning = " returning %s" % ",".join(map(str,returning))
		else:
			_returning = " returning %s" % str(returning)

	def _fmt(c,s):
		if c in rawcols: 
			return s
		return sqlize(s)

	return "insert into %s (%s) values (%s) %s;" % (str(table), ",".join(_cols), ",".join([_fmt(col,data[col]) for col in _cols]),_returning)

def qupd(table, data, keys, returning=None, cols=None, rawcols=[], diff=False, excludeColsDiff=[]):
	"""
	Renvoie une requête update faite à partir des arguments
	
	@param table: le nom de la table
	@type table: string
	@param data: le dictionnaire des données à mettre à jour
	@type data: dict
	@param keys: la liste des clés utilisée dans la contrainte. La valeur de ces clés doit se trouver dans data
	@type keys: list
	@param cols: liste des colonnes SQL à prendre en compte dans la requête update. Par défault les clés de data sont prises en compte
	pour la génération de la requête
	@type cols: list
	@param rawcols: liste des colonnes SQL à prendre sans aucun traitement particulier
	@type rawcols: list
	@rtype: string
	@return: la requête SQL update générée
	"""
	return qupd82(table, data, keys, returning=returning, cols=cols, rawcols=rawcols, diff=diff, excludeColsDiff=excludeColsDiff)

def qupd82(table, data, keys, returning=None, cols=None, rawcols=[], diff=False, excludeColsDiff=[]):
	"""
	Renvoie une requête update faite à partir des arguments
	
	@param table: le nom de la table
	@type table: string
	@param data: le dictionnaire des données à mettre à jour
	@type data: dict
	@param keys: la liste des clés utilisée dans la contrainte. La valeur de ces clés doit se trouver dans data
	@type keys: list
	@param cols: liste des colonnes SQL à prendre en compte dans la requête update. Par défault les clés de data sont prises en compte
	pour la génération de la requête
	@type cols: list
	@param rawcols: liste des colonnes SQL à prendre sans aucun traitement particulier
	@type rawcols: list
	@rtype: string
	@return: la requête SQL update générée
	"""

	if su_.isBlank(table) or len(data) == 0 or len(keys) == 0 or not isinstance(data,types.DictType) or (not isinstance(keys,types.ListType) and not isinstance(keys,sets.Set)):
                return None
	flag = True
	for elem in data.keys():
		if not elem in keys:
			flag = False
	if flag:
		return None

	_cols = cols
	if _cols is None:
		_cols = data.keys()

	_returning = ""
	if returning is not None:
		if type(returning) == list:
			_returning = " returning %s" % ",".join(map(str,returning))
		else:
			_returning = " returning %s" % str(returning)

	def _fmt(c,s):
		if c in rawcols:
			return s
		return sqlize(s)

	setvals = ",".join(["%s=%s" % (col,_fmt(col,data[col])) for col in filter(lambda x: x not in keys, _cols)])
	keyvals = " and ".join(["%s=%s" % (key,_fmt(key,data[key])) for key in keys])
	if diff:
		diffvals = " and ( " + " or ".join(["%s!=%s" % (col,_fmt(col,data[col])) for col in filter(lambda x: x not in keys and x not in excludeColsDiff, _cols)]) + ")"
	else:
		diffvals = ""
	return "update %s set %s where %s%s%s;" % (str(table), setvals, keyvals, diffvals, _returning)

def sqlize(s, strips=True, quoteNumber=True):
	"""
	formatte une valeur python en SQL. Par exemple None sera transformée en 'NULL'

	@param s: la chaîne de caractères à traiter
	@param strips: option pour supprimer ou non les blancs à gauche et à droite
	@param quoteNumber: option pour mettre des quotes autour des nombres (int, float, ...)
	@type s: string
	@rtype: string
	@return: la chaîne de caractères SQL
	"""

	if s is None:
		return "NULL"

	typs = type(s)

	if typs in [float, int]:
		if quoteNumber:
			return "'%s'" % s
		return '%s' % s

	if typs == long:
		try:
			if quoteNumber:
				return "'%d'" % int(s)
			return '%d' % int(s)
		except:
			if quoteNumber:
				return "'%s'" % s
			return '%s' % s

	if strips:
		ss=su_.a2unicode(s).strip()
	else:
		ss=su_.a2unicode(s)

	ssu = ss.upper()
	if typs in [list, tuple, sets]:
		return "(%s)" % ",".join(map(lambda selt: sqlize(selt, strips=strips, quoteNumber=quoteNumber), s))

	if ssu in ["FALSE", "TRUE", "DEFAULT"]:
		return ssu
	if ssu.startswith("NOW()"):
		return s
	return "'%s'" % ss.replace("\\","/").replace("'","''")

def startswith(k,v,nullable=True):
	"""
	Renvoie une chaîne SQL permettant de tester si les valeurs de la colonne k commencent par la valeur v.
	
	@param k: la colonne SQL à tester
	@type k: string
	@param v: la valeur à tester
	@type v: string
	@rtype: string
	@return: une chaîne SQL
	@warning: attribut nullable non géré
	"""

	if (not nullable and v == None) or su_.isBlank(k) or su_.isBlank(v):
		return None
	fmt_v = sqlize('%s%%' % v)
	return " upper(%s::text) LIKE upper(%s::text) " % (k, fmt_v)

