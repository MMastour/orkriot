# coding: utf-8

from datetime import date, datetime, time, timedelta
from decimal import Decimal
import logging
import momoko
import psycopg2
import traceback
from psycopg2.extras import RealDictConnection, RealDictCursor
from tornado import gen
from tornado.locks import Event
from orkit.utils.jsonutils import json_encode


#DEC2FLOAT = psycopg2.extensions.new_type(
#    psycopg2.extensions.DECIMAL.values,
#    'DEC2FLOAT',
#    lambda value, curs: float(value) if value is not None else None)
#psycopg2.extensions.register_type(DEC2FLOAT)

def _fmt(md, c, v):
	if v == '': # non text vide => null
		_colmeta = md['columns'][c]
		if not _colmeta['notnull']:
			_typ = _colmeta['type'].lower()
			if 'char' not in _typ and 'text' not in _typ:
				return None
	else:
		_colmeta = md['columns'][c]
		_typ = _colmeta['type'].lower()
		if 'bool' in _typ and type(v) != bool: # int => bool
			return bool(v)
		if _typ == "date" and v:
			if isinstance(v, datetime):
				v = str(v)
			elif isinstance(v, date):
				v = str(v)
			v = v.replace('/','-')
			if v.find('-') != 4: # date en français
				v = "-".join(v.split('-')[::-1])
	return v

def build_dsn(opts):
	"""
	renvoie un dsn pour la db et gérant un bug sur les mdp vides.
	"""
	# dirty fix erreur double :
	# - erreur si host n'est pas localhost et mdp vide sans quotes
	# - erreur si host est localhost et mdp quoté
	if opts['dbhost'] in ['localhost', '127.0.0.1']:
		dsn = 'dbname=%(dbname)s user=%(dbuser)s password=%(dbpwd)s host=%(dbhost)s port=%(dbport)d' % opts
	else:
		dsn = 'dbname=%(dbname)s user=%(dbuser)s password="%(dbpwd)s" host=%(dbhost)s port=%(dbport)d' % opts
	logging.debug("DSN :::  %s", dsn)
	return dsn

class DB(object):
	def __init__(self, dbuser, dbpwd, dbname, dbhost, dbport, **kwargs):
		self.init_pool(dbuser, dbpwd, dbname, dbhost, dbport, **kwargs)

	def init_pool(self, dbuser, dbpwd, dbname, dbhost, dbport, **kwargs):
		size = kwargs.get('size', 1)
		maxSize = max(size, kwargs.get('max_size', 30))
		self.db = momoko.Pool(
			connection_factory=RealDictConnection,
			dsn=build_dsn(locals()),
			size=size,
			max_size=maxSize,
			setsession=("SET TIME ZONE UTC",),
			cursor_factory=RealDictCursor,
			raise_connect_errors=True,
		)
		self.defaultSchema = kwargs.get('defaultSchema', 'public')
		self.schemas = {}
		# Ajout d'un flag tornado pour contrôler le chargement complet des metadata
		# Un problème pouvait se produire lorsque plusieurs reqûetes arrivaient au même moment
		# La première commence a charger les données et la seconde récupère le dictionnaire avant que toutes
		# les infos aient pu être chargés
		self.flagMetadataLoaded = None

	@gen.coroutine
	def connect(self):
		""" Méthode permettant de se connecter à la base de donnée. A faire avant toute action sur la bdd """
		yield self.db.connect()
		self.db.connected = True

	version = None
	@gen.coroutine
	def getversion(self, conn=None):
		""" Méthode permettant d'obtenir la version de posgresql """
		releaseConn = False
		if not self.version:
			#Exception happens when connection is lost : fixed in momoko 2.2
			try:
				if(not self.db.connected):
					yield self.connect()
				if conn is None:
					conn = yield self.getConn()
					releaseConn = True
			except Exception as error:
				logging.error(error)
				raise gen.Return(None)
			try:
				cursor = yield conn.execute("SHOW server_version")
				assert cursor
			except (psycopg2.Warning, psycopg2.Error) as error:
				logging.error(error)
				DB.version = None
			else:
				result = cursor.fetchone()
				self.version = result.values()[0]
		if releaseConn:
			self.db.putconn(conn)
		raise gen.Return(self.version)

	@gen.coroutine
	def get_current_dbname(self):
		r = yield self.first("SELECT current_database()")
		raise gen.Return(r)

	@gen.coroutine
	def is_version_equals_or_sup(self, v, conn=None):
		version = (yield self.getversion(conn=conn)).split(".")
		v = v.split(".")
		for i in range(min(len(version), len(v))):
			if int(version[i]) < int(v[i]):
				raise gen.Return(False)
			elif int(version[i]) > int(v[i]):
				raise gen.Return(True)
		#Equal ?
		if len(v) > len(version):
			raise gen.Return(False)
		else:
			raise gen.Return(True)

	@gen.coroutine
	def close(self):
		""" Méthode permettant de fermer toutes les connexions à la base de donnée. A faire une fois que toutes les opérations sont terminées """
		self.db.close()
		self.db.connected = False

	@gen.coroutine
	def metadata(self, schema='public', force=False, conn=None):
		# Au premier passage l'evenement est initialisé, puis pour tous les suivants
		# la condition doit être remplie pour continuer
		if(not self.db.connected):
			yield  self.connect()
		if self.flagMetadataLoaded is None:
			self.flagMetadataLoaded = Event()
		else :
			yield self.flagMetadataLoaded.wait()

		if not force and schema in self.schemas:
			raise gen.Return(self.schemas[schema])

		data = self.schemas[schema] = {}

		# Lists the names of the tables in the database
		__tablenames_q = """SELECT c.relname as name
						  FROM pg_class c
						  JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
						  LEFT JOIN pg_user u ON c.relowner = u.usesysid
						  WHERE c.relkind IN ('r','')
							AND c.relname !~ '^pg_'
							AND c.relname !~ '^sql_'
							AND n.nspname = '%s'
						  ORDER BY 1;""" % schema

		result = yield self.select(__tablenames_q, conn=conn)
		for r in result:
			if(not isinstance(r,dict)) :
				raise Exception("UnexpectedTypeError","Query result item is not a dict. Check PostgreSQL adapter version")
			data[r['name']] = { 'columns': {}, 'pkey': [], 'fkey': {}, 'defaults': [], 'notnulls': [] }

		# The following returns a set of rows with null seperated fields.
		__foreign_keys_q = """SELECT c.relname tbl,
			   a.attname as tblcol,
			   cf.relname as ftbl,
			   af.attname as ftblcol
		  FROM pg_attribute AS af ,
			   pg_attribute AS a,
			   ( SELECT conrelid,
						confrelid,
						conkey[i] AS conkey,
						confkey[i] as confkey
				   FROM ( SELECT conrelid,
								 confrelid,
								 conkey,
								 confkey,
								 generate_series(1, array_upper(conkey, 1)) AS i
							FROM pg_constraint c
							JOIN pg_catalog.pg_namespace n ON n.oid = c.connamespace
			 				WHERE contype = 'f'
			 				AND n.nspname = '%(schema)s'
				 ) AS ss
			   ) AS ss2
			   LEFT JOIN pg_class cf ON ss2.confrelid = cf.oid
			   LEFT JOIN pg_class c ON ss2.conrelid = c.oid
		 WHERE af.attnum = confkey
		   AND af.attrelid = confrelid
		   AND a.attnum = conkey
		   AND a.attrelid = conrelid""" % dict(schema=schema)
		result = yield self.select(__foreign_keys_q, conn=conn)
		for r in result:
			tbl = r['tbl']
			dfkey = data[tbl]['fkey']
			dfkey[r['tblcol']] = {'tbl': r['ftbl'], 'col': r['ftblcol']}

		# The attributes of a table given the tablename
		for table in data.keys():
			__attributes_q = """SELECT a.attname as name
			FROM pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace,
			pg_attribute a
			WHERE c.relname = '%(table)s'
			AND n.nspname = '%(schema)s'
			AND a.attnum > 0 AND a.attrelid = c.oid
			ORDER BY a.attnum""" % dict(schema=schema, table=table)
			result = yield self.select(__attributes_q, conn=conn)
			columns = data[table]['columns']
			for r in result:
				columns[r['name']] = {'notnull': False, 'default': None}

		# Attributes and types given the tablename
		for table in data.keys():
			__atts_with_types_q = """SELECT a.attname as name, format_type(a.atttypid, a.atttypmod) as type
			FROM pg_class c
			JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace,
			pg_attribute a
			WHERE c.relname = '%(table)s'
			AND n.nspname = '%(schema)s'
			AND a.attnum > 0 AND a.attrelid = c.oid
			ORDER BY a.attnum""" % dict(schema=schema, table=table)
			result = yield self.select(__atts_with_types_q, conn=conn)
			columns = data[table]['columns']
			for r in result:
				columns[r['name']]['type'] = r['type']

		for table in data.keys():
			__atts_with_defaults_q = """SELECT a.attname as name,
			replace(substring(d.adsrc,0,128),'''','') as "default"
			FROM pg_attrdef d,
			pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace,
			pg_attribute a
			WHERE c.relname = '%(table)s' AND c.oid = d.adrelid
			AND n.nspname = '%(schema)s'
			and c.oid=a.attrelid and d.adnum=a.attnum""" % dict(schema=schema, table=table)
			result = yield self.select(__atts_with_defaults_q, conn=conn)
			columns = data[table]['columns']
			defaults = data[table]['defaults']
			for r in result:
				name = r['name']
				columns[name]['default'] = r['default']
				defaults = name

		for table in data.keys():
			__atts_with_notnulls_q = """SELECT a.attname as name,
			a.attnotnull as notnull
			FROM pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace,
			pg_attribute a
			WHERE c.relname = '%(table)s' AND a.attrelid = c.oid
			AND n.nspname = '%(schema)s'
			AND a.attnum > 0 AND a.attnotnull IS TRUE
			ORDER BY a.attnum""" % dict(schema=schema, table=table)
			result = yield self.select(__atts_with_notnulls_q, conn=conn)
			columns = data[table]['columns']
			notnulls = data[table]['notnulls']
			for r in result:
				name = r['name']
				columns[name]['notnull'] = True
				notnulls.append(name)

		# The primary keys of a table given the tablename
		for table in data.keys():
			__primary_keys_q = """SELECT a.attname as name
			FROM pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace,
			pg_attribute a, pg_index i
			WHERE c.relname = '%(table)s' and c.oid=i.indrelid
			AND n.nspname = '%(schema)s'
			AND a.attnum > 0 AND a.attrelid = i.indexrelid
			and i.indisprimary='t'""" % dict(schema=schema, table=table)
			result = yield self.select(__primary_keys_q, conn=conn)
			columns = data[table]['columns']
			pkey = data[table]['pkey']
			for r in result:
				name = r['name']
				columns[name]['pkey'] = True
				pkey.append(name)

		#Initialise le flag à TRUE
		self.flagMetadataLoaded.set()	

		raise gen.Return(data)

	@gen.coroutine
	def debug(self, query, conn, **params):
		"""Renvoie requête sql."""
		sql = conn.mogrify(query, parameters=params)
		raise gen.Return(sql)

	@gen.coroutine
	def first(self, query, conn=None, **params):
		"""Renvoie le premier élément d'un select."""
		try:
			if(not self.db.connected):
				yield self.connect()
		except Exception as error:
			logging.error(error)
			raise gen.Return(None)

		result = yield self.select(query=query, conn=conn, **params)
		if result:
			raise gen.Return(result[0])
		raise gen.Return(None)

	@gen.coroutine
	def getConn(self):
		try:
			conn = yield self.db.getconn()
		except Exception, error:
			#Tentative de reconnexion :  nécessaire lorsque le serveur posrgres a été redémarré
			try:
				if not self.db.closed:
					yield self.close()
				yield self.connect()
				conn = yield self.db.getconn()
			except:
				raise Exception(error)
		raise gen.Return(conn)

	@gen.coroutine
	def putConn(self, conn):
		try:
			self.db.putconn(conn)
		except Exception as error:
			logging.error(error)

	@gen.coroutine
	def raw(self, query, reterr=False, debug=False, conn=None, **params):
		"""Exécute un SQL tel quel avec renvoie ou non d'un résultat."""
		releaseConn = False
		try:
			if(not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			if reterr:
				raise gen.Return((0, error))
			raise gen.Return(0)
		try:
			#cursor = yield momoko.Op(self.db.execute, query, params)
			if debug:
				logging.debug("raw execute : %s", query)
			cursor = yield conn.execute(query, parameters=params)
			assert cursor
		except (psycopg2.Warning, psycopg2.Error) as error:
			logging.error(error)
			if reterr:
				ret = (0, error)
			else:
				ret = None
		else:
			try:
				count = len(cursor.fetchall())
			except: # pas de résultat
				count = 0
			if reterr:
				ret = (count, None)
			else:
				ret = count
		finally:
			if releaseConn:
				self.db.putconn(conn)

		raise gen.Return(ret)

	@gen.coroutine
	def select(self, query, reterr=False, debug=False, conn=None, **params):
		releaseConn = False
		try:
			if(not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			if reterr:
				raise gen.Return( (0, error) )
			raise gen.Return(None)
		r = None
		try:
			#cursor = yield momoko.Op(self.db.execute, query, params)
			#parameters = params.items()
			if debug:
				logging.debug("select execute : %s", query)
			cursor = yield conn.execute(query, parameters=params)
			assert cursor
		except (psycopg2.Warning, psycopg2.Error) as error:
			if query:
				logging.error(query)
			logging.error(error)
			if reterr:
				raise gen.Return( (0, error) )
			raise gen.Return(None)
		else:
			r = cursor.fetchall()
		finally:
			if releaseConn:
				self.db.putconn(conn)
		if reterr:
			raise gen.Return( (r, None) )
		raise gen.Return(r)

	@gen.coroutine
	def delete(self, table, datas=None, schema='public', returning=None, debug=False, reterr=False, conn=None):
		ret = []
		releaseConn = False
		try:
			if (not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			ret.append((None, error) if reterr else None)
			raise gen.Return(ret)

		md = (yield self.metadata(schema=schema, conn=conn)).get(table, None)
		if md is None:
			logging.error("Table %s doesn't exist in schema %s" % (table, schema))
			if reterr:
				ret.append((None, "Table %s doesn't exist in schema %s" % (table, schema)))
			else:
				ret.append(None)
			if releaseConn:
				self.db.putconn(conn)
			raise gen.Return(ret)

		columns = md['columns'].keys()
		pkey = "idobj"
		if returning:
			if type(returning) == list:
				preturning = "RETURNING %s" % ",".join(returning)
			else:
				preturning = "RETURNING %s" % returning
		else:
			preturning = ""
		if datas :
			for data in datas:
				try:
					params = dict(map(lambda c: (c, data[c]), filter(lambda name: name in columns, data.keys())))
					pcols = filter(lambda c: c in pkey, params.keys())
					pcols = " AND ".join(map(lambda c: "%s=%%(%s)s" % (c,c), pcols))
					query = "DELETE FROM %s.%s WHERE %s %s;" % (schema, table, pcols, preturning)
					#cursor = yield momoko.Op(self.db.execute, query, params)
					if debug:
						logging.debug("delete execute : %s", query)
					cursor = yield conn.execute(query, parameters=params)
					assert cursor
				except (psycopg2.Warning, psycopg2.Error) as error:
					logging.error(error)
					logging.error("pkey : %s",pkey)
					logging.error("columns : %s", columns)
					if reterr:
						ret.append((None, error))
					else:
						ret.append(None)
				else:
					if returning:
						a = cursor.fetchone()
						if type(returning) == list:
							a = dict(map(lambda c: (c, a[c]), returning))
						if reterr:
							ret.append((a, None))
						else:
							ret.append(a)
					else:
						if reterr:
							ret.append((True, None))
						else:
							ret.append(True)
		else:
			try:
				query = "DELETE FROM %s.%s %s;" % (schema, table, preturning)
				if debug:
					logging.debug("delete execute : %s", query)
				cursor = yield conn.execute(query)
				assert cursor
			except (psycopg2.Warning, psycopg2.Error) as error:
				logging.error(error)
				if reterr:
					ret.append( (None, error ))
				else:
					ret.append(None)
			else:
				a = True
				if returning:
					a = cursor.fetchone()
					if type(returning) == list:
						a = dict(map(lambda c: (c, a[c]), returning))

				if reterr:
					ret.append( (a, None) )
				else:
					ret.append(a)

		if releaseConn:
			self.db.putconn(conn)
		raise gen.Return(ret)

	@gen.coroutine
	def insert(self, table, datas, schema='public', returning='*', debug=False, reterr=False, conn=None):
		ret = []
		releaseConn = False
		try:
			if (not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			ret.append((None, error) if reterr else None)
			raise gen.Return(ret)

		md = (yield self.metadata(schema=schema, conn=conn)).get(table, None)
		if md is None:
			logging.error("Table %s doesn't exist in schema %s" %(table, schema))
			if reterr:
				ret.append((None, "Table %s doesn't exist in schema %s" %(table, schema)))
			else:
				ret.append(None)
			if releaseConn:
				self.db.putconn(conn)
			raise gen.Return(ret)
		columns = md['columns'].keys()
		for data in datas:
			try:
				#params = dict(map(lambda c: (c, data[c]), filter(lambda name: name in columns, data.keys())))
				params = dict(map(lambda c: (c, data[c] if type(data[c]) != dict else json_encode(data[c])), filter(lambda name: name in columns, data.keys())))
				cols = sorted(params.keys())
				pcols = ",".join(map(lambda c: "%%(%s)s" % c, cols))
				cols = ",".join(cols)
				if returning:
					if type(returning) == list:
						preturning = "RETURNING %s" % ",".join(returning)
					else:
						preturning = "RETURNING %s" % returning
				else:
					preturning = ""
				query = "INSERT INTO %s.%s (%s) VALUES (%s) %s;" % (schema, table, cols, pcols, preturning)
				
				if debug:
					dbg = yield self.debug(query, conn, **params)
					logging.info(dbg)
				cursor = yield conn.execute(query, parameters=params)
				assert cursor
			except (psycopg2.Warning, psycopg2.Error) as error:
				logging.error(error)
				if reterr:
					ret.append((None, error))
				else:
					ret.append(None)
			else:
				if returning:
					a = cursor.fetchone()
					if type(returning) == list:
						a = dict(map(lambda c: (c, a[c]), returning))
					if reterr:
						ret.append((a, None))
					else:
						ret.append(a)
				else:
					if reterr:
						ret.append((None, None))
					else:
						ret.append(None)
		if releaseConn:
			self.db.putconn(conn)
		raise gen.Return(ret)

	@gen.coroutine
	def update(self, table, datas, keys=None, schema='public', reterr=False, returning='*', debug=False, conn=None):
		ret = []
		releaseConn = False
		try:
			if (not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			ret.append((None, error) if reterr else None)
			raise gen.Return(ret)

		md = (yield self.metadata(schema=schema, conn=conn)).get(table, None)
		if md is None:
			logging.error("Table %s doesn't exist in schema %s" % (table, schema))
			if reterr:
				ret.append((None, "Table %s doesn't exist in schema %s" % (table, schema)))
			else:
				ret.append(None)
			if releaseConn:
				self.db.putconn(conn)
			raise gen.Return(ret)
		columns = md['columns'].keys()
		pkey = "idobj"

		for data in datas:
			try:
				#params = dict(map(lambda c: (c, _fmt(md, c, data[c])), filter(lambda name: name in columns, data.keys())))
				params = dict(map(lambda c: (c, _fmt(md, c, data[c] if type(data[c]) != dict else json_encode(data[c]))), filter(lambda name: name in columns, data.keys())))
				cols = sorted(params.keys())
				pcols = filter(lambda c: c in pkey, cols)
				cols = ",".join(map(lambda c: "%s=%%(%s)s" % (c,c), cols))
				if not keys:
					keys = pcols
				where = " AND ".join(map(lambda c: "%s=%%(%s)s" % (c,c), keys))

				if returning:
					if type(returning) == list:
						preturning = "RETURNING %s" % ",".join(returning)
					else:
						preturning = "RETURNING %s" % returning
				else:
					preturning = ""
				query = "UPDATE %s.%s SET %s WHERE %s %s;" % (schema, table, cols, where, preturning)
				#cursor = yield momoko.Op(self.db.execute, query, params)
				if debug:
					dbg = yield self.debug(query, conn, **params)
					logging.debug(dbg)
				cursor = yield conn.execute(query, parameters=params)
				assert cursor
			except (psycopg2.Warning, psycopg2.Error) as error:
				if query:
					logging.error(query)
				logging.error(error)
				if reterr:
					ret.append((None, error))
				else:
					ret.append(None)
			else:
				if returning:
					a = cursor.fetchone()
					if type(returning) == list:
						a = dict(map(lambda c: (c, a[c]), returning))
					if reterr:
						ret.append((a, None))
					else:
						ret.append(a)
				else:
					if reterr:
						ret.append((None, None))
					else:
						ret.append(None)

		if releaseConn:
			self.db.putconn(conn)
		raise gen.Return(ret)

	@gen.coroutine
	def upsert(self, table, datas, schema='public', reterr=False, returning='*', debug=False, conn=None):
		"""
		update data if exists else insert
		:param table: name of table
		:param datas: list of dictionnary contains data
		:param schema:
		:param returning: returning data
		:return:
		"""
		ret = []
		releaseConn = False
		try:
			if (not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			ret.append((None, error) if reterr else None)
			raise gen.Return(ret)

		if (yield self.is_version_equals_or_sup("9.1", conn=conn)):
			#Upsert is supported since 9.1
			md = (yield self.metadata(schema=schema, conn=conn)).get(table, None)
			if md is None:
				logging.error("Table %s doesn't exist in schema %s" % (table, schema))
				if reterr:
					ret.append((None, "Table %s doesn't exist in schema %s" % (table, schema)))
				else:
					ret.append(None)
				if releaseConn:
					self.db.putconn(conn)
				raise gen.Return(ret)
			columns = md['columns'].keys()
			pkey = md['pkey']

			for data in datas:
				try:
					#params = dict(map(lambda c: (c, _fmt(md, c, data[c])), filter(lambda name: name in columns, data.keys())))
					params = dict(map(lambda c: (c, _fmt(md, c, data[c] if type(data[c]) != dict else json_encode(data[c]))), filter(lambda name: name in columns, data.keys())))
					cols = sorted(params.keys())
					pcols = filter(lambda c: c in pkey, cols)
					setcols = ",".join(map(lambda c: "%s=%%(%s)s" % (c,c), cols))
					values = ",".join(map(lambda c: "%%(%s)s" % c, cols))
					pncols = ",".join(pcols)
					pcolsdet = " AND ".join(map(lambda c: "%s.%s.%s=%%(%s)s" % (schema, table, c,c), pcols))
					pcols = " AND ".join(map(lambda c: "%s=%%(%s)s" % (c,c), pcols))

					cols = ",".join(cols)

					if (yield self.is_version_equals_or_sup("9.5", conn=conn)):
						#New way upsert since postgresl 9.5
						if returning:
							if type(returning) == list:
								preturning = "RETURNING %s" % ",".join(returning)
							else:
								preturning = "RETURNING %s" % returning
						else:
							preturning = ""

						query = "INSERT INTO %(schema)s.%(table)s (%(cols)s) VALUES (%(values)s) ON CONFLICT (%(pncols)s) " \
								 "DO UPDATE SET %(setcols)s WHERE %(pcolsdet)s %(ret)s"\
								 % (dict(schema=schema, table=table, cols=cols, values=values, pncols=pncols, setcols=setcols, pcolsdet=pcolsdet, ret=preturning))
					else:
						#Between 9.1 and 9.5
						#The old way with Common Table Expressions (CTEs) since PostgreSQL version 9.1
						query = "WITH UPSERT AS (UPDATE %s.%s SET %s WHERE %s RETURNING %s.*)" % (schema, table, setcols, pcols, table)
						if returning:
							preturning = ",".join(returning) if type(returning) is list else str(returning)
							query += ",INSERTED AS (INSERT INTO %s.%s (%s) SELECT %s WHERE NOT EXISTS (SELECT 1 FROM UPSERT WHERE %s) RETURNING *)" % (schema, table, cols, values, pcols)
							query += "SELECT %s FROM UPSERT UNION ALL SELECT %s FROM INSERTED" % (preturning, preturning)
						else:
							query += "INSERT INTO %s.%s (%s) SELECT %s WHERE NOT EXISTS (SELECT 1 FROM UPSERT WHERE %s)" % (schema, table, cols, values, pcols)

					if debug:
						dbg = yield self.debug(query, conn, **params)
						logging.debug(dbg)

					#cursor = yield momoko.Op(self.db.execute, query, params)
					cursor = yield conn.execute(query, parameters=params)
					assert cursor
				except (psycopg2.Warning, psycopg2.Error) as error:
					logging.error(error)
					if reterr:
						ret.append( (None, error ))
					else:
						ret.append(None)
				else:
					a = None
					if returning:
						a = cursor.fetchone()
						if type(returning) == list:
							a = dict(map(lambda c: (c, a[c]), returning))
						if reterr:
							ret.append( (a, None) )
						else :
							ret.append(a)
		else:
			version = yield self.getversion(conn=conn)
			logging.error("This function is not available on your postgresql server, you need at least 9.1 and you have %s", version)
			#TODO Do select then insert or update

		if releaseConn:
			self.db.putconn(conn)
		raise gen.Return(ret)

	@gen.coroutine
	def qins(self, table, datas, schema='public', returning='',debug=False, reterr=False, conn=None):
		ret = []
		releaseConn = False
		try:
			if (not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			ret.append((None, error) if reterr else None)
			raise gen.Return(ret)
		md = (yield self.metadata(schema=schema, conn=conn)).get(table, None)	
		if md is None:
			logging.error("Table %s doesn't exist in schema %s" % (table, schema))
			if reterr:
				ret.append((None, "Table %s doesn't exist in schema %s" %(table, schema)))
			else:
				ret.append(None)
			if releaseConn:
				self.db.putconn(conn)
			raise gen.Return(ret)
		columns = md['columns'].keys()
		for data in datas:
			try:
				params = dict(map(lambda c: (c, data[c] if type(data[c]) != dict else json_encode(data[c])), filter(lambda name: name in columns, data.keys())))
				cols = sorted(params.keys())
				pcols = ",".join(map(lambda c: "%%(%s)s" % c, cols))
				cols = ",".join(cols)
				if returning:
					if type(returning) == list:
						preturning = "RETURNING %s" % ",".join(returning)
					else:
						preturning = "RETURNING %s" % returning
				else:
					preturning = ""
				query = "INSERT INTO %s.%s (%s) VALUES (%s) %s;" % (schema, table, cols, pcols, preturning)
				sql = conn.mogrify(query, parameters=params)
				ret.append(sql+"\n")
			except (psycopg2.Warning, psycopg2.Error) as error:
				logging.error(error)

		if releaseConn:
			self.db.putconn(conn)
		raise gen.Return(ret)

	@gen.coroutine
	def qupd(self, table, datas, schema='public', returning='',debug=False, reterr=False, conn=None):
		ret = []
		releaseConn = False
		try:
			if (not self.db.connected):
				yield self.connect()
			if conn is None:
				conn = yield self.getConn()
				releaseConn = True
		except Exception as error:
			logging.error(error)
			ret.append((None, error) if reterr else None)
			raise gen.Return(ret)
		md = (yield self.metadata(schema=schema, conn=conn)).get(table, None)
		#md = (yield self.metadata(schema=schema))[table]
		if md is None:
			logging.error("Table %s doesn't exist in schema %s" % (table, schema))
			if reterr:
				ret.append((None, "Table %s doesn't exist in schema %s" % (table, schema)))
			else:
				ret.append(None)
			if releaseConn:
				self.db.putconn(conn)
			raise gen.Return(ret)
		columns = md['columns'].keys()
		pkey = md['pkey']

		for data in datas:
			try:
				params = dict(map(lambda c: (c, _fmt(md, c, data[c] if type(data[c]) != dict else json_encode(data[c]))), filter(lambda name: name in columns, data.keys())))
				cols = sorted(params.keys())
				pcols = filter(lambda c: c in pkey, cols)
				cols = ",".join(map(lambda c: "%s=%%(%s)s" % (c,c), cols))
				pcols = " AND ".join(map(lambda c: "%s=%%(%s)s" % (c,c), pcols))
				if returning:
					if type(returning) == list:
						preturning = "RETURNING %s" % ",".join(returning)
					else:
						preturning = "RETURNING %s" % returning
				else:
					preturning = ""
				query = "UPDATE %s.%s SET %s WHERE %s %s;" % (schema, table, cols, pcols, preturning)
				sql = conn.mogrify(query, parameters=params)
				ret.append(sql+"\n")
			except (psycopg2.Warning, psycopg2.Error) as error:
				logging.error(error)

		if releaseConn:
			self.db.putconn(conn)

		raise gen.Return(ret)




if __name__ == "__main__":
	dbuser = 'root'
	dbpwd = ''
	dbhost = 'localhost'
	dbport = 5432
	# dbport = 5433		# postgresql 9.5
	#dbname = 'orkomgks'
	dbname = 'orkaisse'

	db = DB(dbuser, dbpwd, dbname, dbhost, dbport)

	import time
	import tornado
	from datetime import datetime

	@gen.coroutine
	def main():
		""" Tests sur une base de données orkarte """

		### Méthode à appeler avant toute connexion à la bdd
		#yield db.connect()

		# print (yield db.metadata())
		# print (yield db.insert('b_tva', datas=[dict(id='13', libelle='zango')]))
		# print (yield db.select("select * from b_tva where id='13'"))
		# print (yield db.select("select * from b_tva where id='13'"))
		# print (yield db.update('b_tva', datas=[dict(id='13', libelle='zango %r' % time.time())]))
		# print (yield db.delete('b_tva', datas=[dict(id='13')]))
		# print (yield db.select("select * from b_tva where id='13'"))

		#print (yield db.update('a_id', datas=[dict(ean='3309103800256', spuvttc='zango')], reterr=True))

		datas=[dict(idcmp=10001, idm=0, idens=0, lib='TOTOTO')]
		print (yield db.upsert(table="f_cmp", schema="public", datas=datas, reterr=True, debug=True))

		# schema = 'test'
		# 	   dict(ttt=1, tweet_id=1, pinned_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), user_handle = 'rey')]
		# print (yield db.upsert(table="pinned_tweets", schema=schema, datas=datas, reterr=True, returning=None))
		# print (yield db.upsert(table="pinned_tweets", schema=schema, datas=[dict(tweet_id=2, pinned_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), user_handle = 'kyloren')], reterr=True, returning='tweet_id'))
		# print (yield db.upsert(table="pinned_tweets", schema=schema, datas=[dict(tweet_id=3, pinned_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), user_handle = 'hansolo')], reterr=True, returning=['tweet_id', 'pinned_at']))
		# print (yield db.upsert(table="pinned_tweets", datas=[dict(tweet_id=4, pinned_at=datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"), user_handle = 'hansolo')]))

		# print (yield db.delete(table="pinned_tweets", schema=schema))

		### Méthode à appeler une fois toutes les opérations terminées
		yield db.close()

	ioloop = tornado.ioloop.IOLoop.instance()
	ioloop.run_sync(main)
