# -*- coding: utf-8 -*-

"""Outils pour manipuler des chaînes de caractères."""

import sets
import unicodedata

def a2ascii(s):
	""" 
	Conversion d'une chaine en ascii

	@param s: la chaîne de caractères à convertir
	@type s: string
	@rtype: string
	@return: la chaîne convertie en ASCII
	"""

	if s is None:
		return None
	if isBlank(s) and len(s.strip()) == 0:
		return ""
	ss = str(s)
	if isinstance(ss,str):
		ss = unicode(ss,"utf8","replace")
	ss = unicodedata.normalize('NFD',ss)
	return ss.encode('ascii','ignore')

def a2bool(value):
	"""
	Transforme une chaîne de caractères en boolean.
	
	@param value: la chaîne de caractères à transformer
	@type value: string
	@rtype: string
	@return: le boolean
	"""

	return ( value is not None and str(value).strip().upper() in ['1', 'ON', 'YES', 'T', 'TRUE'])

def a2s(obj):
	"""
	Renvoie toute valeur sous forme de chaîne de caractères sans les blancs à gauche et à droite (None renvoie '').
	
	@param obj : la chaîne de caractères à convertir
	@type obj : object
	@rtype : string
	@return : obj transformé en chaîne de caractères
	"""
	if obj is None:
		return ""
	return str(obj).strip()

def a2unicode(obj):
	""" 
	Conversion d'une valeur en unicode

	@param obj: la valeur de caractères à convertir
	@type obj: string
	@rtype: string
	@return: la chaîne convertie en Unicode
	"""

	if not isinstance(obj,(str,unicode)): obj=str(obj) 
	if isinstance(obj,unicode): obj=obj.encode("utf8","xmlcharrefreplace")
	return obj

def isBlank(o):
	"""
	Tester si la représentation d'une variable est None ou ne contient que des blancs (espace, tabulation, retour à la ligne, ...).
	exemple 1 : isBlank(None), isBlank(""), isBlank(" "), isBlank("\\t \\n") renvoient True
	exemple 2 : isblank(" isNotBlank"), isBlank(" \\t test") renvoient False

	@param o: la chaîne de caractères à tester
	@type o: string
	@rtype: bool
	@return: vrai si la chaine est vide, faux sinon
	"""

	return o == None or len(str(o).strip()) == 0

def isNotBlank(o):
        """
        Tester si la représentation d'une variable n'est pas None ou ne contient pas que des blancs (espace, tabulation, retour à la ligne, ...).
        exemple 1 : isblank(" isNotBlank"), isBlank(" \\t test") renvoient True
        exemple 2 : isBlank(None), isBlank(""), isBlank(" "), isBlank("\\t \\n") renvoient False
        """
        return not isBlank(o)

def parseBool(value):
	"""
	Renvoie la représentation en chaîne de caractères d'un booléen, ou '' si la valeur est None
	
	@param value: la chaîne de caractères à parser
	@type value: string
	@rtype: string
	@return: la représentation chaîne du booléen 
	"""

	if str(value).upper() in ['1', 'ON', 'YES', 'T', 'TRUE']:
		return 'true'
	elif str(value).upper() in ['0','OFF', 'NO', 'F', 'FALSE']:
		return 'false'
	return ''

def s2utf8(a): 
	"""Encode toutes les valeurs de type str déduites de a en en unicode utf-8 puis renvoie le résultat de a.
	str -> utf-8
	dict -> valeurs de type str -> utf8
	list,set,tuple -> valeurs de type str -> utf8
	"""
	typa = type(a)
	if typa == str:
		return unicode(a, "utf-8")
	elif typa == dict:
		for k in a.keys():
			a[k] = s2utf8(a[k])
        elif typa == list:
		return map(s2utf8,a)
	elif typa == tuple:
		return tuple(map(s2utf8,a))
        elif typa == sets.Set:
		return sets.Set(map(s2utf8,a))
        return a

