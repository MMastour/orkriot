# coding: utf-8

def intify(val, defval=0):
	"""
	Return the integer value if possible or the defval else 0
	:param val:
	:param defval:
	:return:
	"""
	try:
		return int(val)
	except:
		return defval

