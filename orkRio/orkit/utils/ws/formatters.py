# coding: utf8

import calendar
from decimal import Decimal
from datetime import datetime, date

MOIS = ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre']

def decimal2(v):
	return Decimal('%.2f' % float(v))

def dateToMoisLAnnee(date):
	"""
	Renvoie une date sous la format mois en lettre + annee
	"""
	try:
		return "%s %s" % (MOIS[date.month - 1], date.year)
	except:
		return ""

def formatMessagePeremption(date):
	""""
	Renvoie le une date sous le format mois+année personnalisé
	TODO : récupérer le format de la date en base 
	"""
	try:
		return "à échanger avant la fin du mois de %s %s" % (MOIS[date.month - 1], date.year)
	except:
		return ""

def postgres_escape_value(v):
	if v is None:
		return "NULL"
	elif type(v) in [int, long, float, Decimal]:
		return v
	elif type(v) in [datetime, date]:
		return "'%s'" % v
	else:
		v = v.replace("'", "''")
		return "'%s'" % v


def add_months(sourcedate,months):
	month = sourcedate.month - 1 + months
	year = sourcedate.year + month / 12
	month = month % 12 + 1
	day = min(sourcedate.day,calendar.monthrange(year,month)[1])
	return date(year,month,day)

def add_years(dt, years):
	"""Renvoie la date donnée décalée de +/- n année (format américain aaaa-mm-jj).
	"""
	an, mois, jour = dt.year, dt.month, dt.day
	return date((an+years), mois, jour)

def report_error(status, errors, error_key, error_msg):
	errors[error_key] = error_msg
	return(status, errors, {})
