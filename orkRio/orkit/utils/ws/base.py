# coding: utf-8
import traceback
import logging
from tornado import gen
from validators import valid_input_data, STATUS_ERROR
from formatters import report_error


class BaseFunction(object):
    """
    Abstract class to control input data and execute a function
    """
    def __init__(self, input_data, action, errors=None):
        self.input_data = input_data
        self.action = action
        self.errors = errors or {}

    @gen.coroutine
    def init(self):
        pass

    def get_checking_data(self):
        return None

    @gen.coroutine
    def control(self, in_authorized_fields=None, in_required_fields=None):
        """
        Méthode de contrôle des données envoyées pour l'action
        :param in_authorized_fields: Controle les champs autorisés en e/s
        :param in_required_fields: Controle les champs autorisés en e/s
        :return: Retourne uniquement les erreurs
        """
        checking_data = self.get_checking_data()
        self.authorized_fields = checking_data and [fieldcheck['field'] for fieldcheck in checking_data] or None

        if not in_authorized_fields:
            in_authorized_fields = self.authorized_fields
            in_required_fields = map(lambda  fieldcheck :fieldcheck['field'] ,filter(lambda x :x.get('required', False), checking_data or []))

        if self.input_data is None:
            return_values = report_error(STATUS_ERROR, self.errors, "__GLOBAL__", 'Input data is None')
            raise gen.Return(return_values)

        if in_authorized_fields:
            if not all(l in in_authorized_fields for l in self.input_data.keys()) :
                return_values = report_error(STATUS_ERROR, self.errors, "__GLOBAL__", 'Unauthorized field detected, input data %s, authorized : %s'% (self.input_data.keys(), in_authorized_fields))
                raise gen.Return(return_values)

        if in_required_fields:
            if not all(l in self.input_data.keys() for l in in_required_fields) :
                return_values = report_error(STATUS_ERROR, self.errors, "__GLOBAL__", 'Required field missing, required : %s' % str(in_required_fields))
                raise gen.Return(return_values)

        # validation
        return_values = None
        if checking_data:
            for fieldcheck in checking_data:
                status, error_key, error = valid_input_data(self.input_data, fieldcheck)
                if status:
                    return_values = report_error(STATUS_ERROR, self.errors,error_key, error)
                    break
        raise gen.Return(return_values)

    @gen.coroutine
    def control_error(self, err):
        raise gen.Return(err)

    @gen.coroutine
    def run(self, **kwargs):
        #Call init function
        yield self.init()
        #Control input data
        err = yield self.control()
        if err:
            yield self.control_error(err)
            raise gen.Return(err)
        #Execute main function
        try:
            res = yield self.execute(**kwargs)
        except:
            traceback.print_exc()
            res = report_error(STATUS_ERROR, self.errors, "__GLOBAL__", 'Internal error function execute()')
        raise gen.Return(res)

    def filter_return_tdata(self, data):
        # Filters output list data
        if len(data):
            for r in data:
                self.filter_return_data(r)

    def filter_return_data(self, data):
        # Filters output data
        for k in data.keys():
            if k not in self.authorized_fields:
                del data[k]

    @gen.coroutine
    def execute(self, **kwargs):
        raise NotImplementedError('subclasses must override execute()!')
