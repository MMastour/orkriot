# coding: utf-8

import traceback
import decimal
import re

STATUS_SUCCESS = 0
STATUS_ERROR = 1
STATUS_IN_DATA_ERR = 100

def is_string(value):
	"""validate if the value is a string"""
	if isinstance(value, str) or isinstance(value, unicode):
		return STATUS_SUCCESS, ''
	else:
		return STATUS_IN_DATA_ERR, 'not a string'

def is_digit(value):
	"""validate if a string contains only digits""" 
	if value.isdigit():
		return STATUS_SUCCESS, ''
	else:
		return STATUS_IN_DATA_ERR, 'not string must contain only digits'

def is_int(value):
	if isinstance(value, int):
		return STATUS_SUCCESS, ''
	else:
		return STATUS_IN_DATA_ERR, 'not a integer'

def is_number(value):
	if type(value) in [ int, float, decimal.Decimal ]:
		return STATUS_SUCCESS, ''
	else:
		return STATUS_IN_DATA_ERR, 'not a number'

def choice_in(value, choices):
	"""validate if the value exists in choices"""
	if value in choices:
		return STATUS_SUCCESS, ''
	else:
		return STATUS_IN_DATA_ERR, 'must be one of %s' % str(choices)


def string_size(value, length):
	"""
	validate if the length of a string value is equal to length.
	length can be an iterable, in which case values of length are OR-tested.
	"""
	try:
		for l in length:
			if len(value) == l:
				return STATUS_SUCCESS, ''				
	except TypeError:
			size(value, [length])	
	return STATUS_IN_DATA_ERR, 'string length must be one of %s' % length
	
def check_ean13(ean):
	"""
	teste si la clé du code ean est valide.
	"""
	if len(ean) != 13:
		return STATUS_SUCCESS, ''
	sean = ean[:12].zfill(12)
	# UPCA/EAN13
	weight=[1,3]*6
	magic=10
	sum = 0
	for i in range(12): # checksum based on first 12 digits.
		sum = sum + int(sean[i]) * weight[i]
	key = str(( magic - (sum % magic) ) % magic)
	if key == ean[-1]:
		return STATUS_SUCCESS, ''
	else:
		return STATUS_IN_DATA_ERR, 'Invalid EAN13 key'

def is_md5(value):
	"""
	teste si une chaîne de caractère peut être un hash md5.
	"""
	status, error = is_string(value)
	if status:
		return status, error
	else:
		check = re.findall(r"([a-fA-F\d]{32})", value)
		if check:
			return STATUS_SUCCESS, ''
		else:
			return STATUS_IN_DATA_ERR, 'string is not a md5 hash'
		
def is_phone_number(value):
	status, error = is_string(value)
	if status:
		return STATUS_IN_DATA_ERR, error
	else:
		check = re.match(r'^[()/.,\-+\s\d]*$', value)
		if check:
			return STATUS_SUCCESS, ''
		else:
			return STATUS_IN_DATA_ERR, 'string is not a phone number'

def is_email_address(value):
	"""
	teste une adresse mail.
	regex empruntée à : http://www.regular-expressions.info/email.html
	"""
	status, error = is_string(value)
	if status:
		return status, error
	else:
		check = re.match(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", value, re.IGNORECASE)
		if check:
			return STATUS_SUCCESS, ''
		else:
			return STATUS_IN_DATA_ERR, 'string is not a valid email address'

def has_date_format(value):
	"""
	teste si un chaîne de caractères est de la forme aaaa-mm-jj.
	"""
	status, error = is_string(value)
	if status:
		return status, error
	else:
		check = re.match(r'^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$', value)
		if check:
			return STATUS_SUCCESS, ''
		else:
			return STATUS_IN_DATA_ERR, 'string is not matching YYYY-MM-DD format'

def is_dictionnary(value, d_keys, d_valid):
	try:
		authorized_keys = set(d_keys)
		required_keys = set(filter(lambda x : d_valid.get(x, {}).get('required', False),d_keys))
		try:
			# if set(value.keys()) != expected_keys:
			# 	return STATUS_ERROR, 'keys for %s should be in %s' % (value, expected_keys)
			if not all(l in authorized_keys for l in value.keys()) :
				return STATUS_IN_DATA_ERR, 'keys for %s should not be in %s' % (value.keys(), authorized_keys)
			elif not all(l in value.keys() for l in required_keys) :
				return STATUS_IN_DATA_ERR, 'keys for %s should be in %s' % (value.keys(), required_keys)
			else:
				for k, v in value.items():
					fieldcheck = d_valid[k]
					f_validators = fieldcheck['validators']
					for validator in f_validators:
						valid_func, valid_kwargs = validator[0], validator[1]
						if valid_kwargs:
							status, error = valid_func(v, **valid_kwargs)
						else:
							status, error = valid_func(v)
						if status:
							error = {k : error}
							return status, error
		except AttributeError:
			traceback.print_exc()
			return STATUS_IN_DATA_ERR, '%s is not a dictionnary' % value
	except TypeError:
		traceback.print_exc()
		return STATUS_IN_DATA_ERR, '%s is not iterable' % value
	return STATUS_SUCCESS, ''


def list_of_dicts(value, d_keys, d_valid):
	try:
		authorized_keys = set(d_keys)
		required_keys = set(filter(lambda x : d_valid.get(x, {}).get('required', False),d_keys))
		for d in value:
			try:
				if not all(l in authorized_keys for l in d.keys()) :
					return STATUS_IN_DATA_ERR, 'keys for %s should not be in %s' % (d_keys, d.keys())
				elif not all(l in d.keys() for l in required_keys) :
					return STATUS_IN_DATA_ERR, 'keys for %s should be in %s' % (d, required_keys)
				else:
					for k, v in d.items():
						fieldcheck = d_valid[k]
						f_validators = fieldcheck['validators']
						for validator in f_validators:
							valid_func, valid_kwargs = validator[0], validator[1]
							if valid_kwargs:
								status, error = valid_func(v, **valid_kwargs)
							else:
								status, error = valid_func(v)
							if status:
								error = {k : error}
								return status, error
			except AttributeError:
				traceback.print_exc()
				return STATUS_IN_DATA_ERR, '%s is not a dictionnary' % d
	except TypeError:
		traceback.print_exc()
		return STATUS_IN_DATA_ERR, '%s is not iterable' % value
	return STATUS_SUCCESS, ''


def valid_input_data(input_data, fieldcheck):
	try:
		fieldname, f_validators, f_required = fieldcheck['field'], fieldcheck['validators'], fieldcheck['required']
		value = input_data.get(fieldname)

		if fieldname in input_data.keys():
			for validator in f_validators:
				v_func, v_args = validator[0], validator[1]

				if v_args:
					status, error = v_func(value, **v_args)
				else:
					status, error = v_func(value)
				if status:
					return status, "%s" % fieldname, error
		return STATUS_SUCCESS, '', ''

	except:
		traceback.print_exc()
		return STATUS_ERROR, "__GLOBAL__", "Unknown error"


