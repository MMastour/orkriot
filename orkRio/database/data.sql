-- ENSEIGNE

INSERT INTO enseigne( idens, nom)VALUES (1, 'marjane');

INSERT INTO enseigne( idens, nom) VALUES (2, 'ElectroPlan');

INSERT INTO enseigne( idens, nom)VALUES (3, 'Acima');


-- MAGASIN

INSERT INTO magasin( idmag, nom, idens)VALUES (1, 'Agadir', 1);

INSERT INTO magasin( idmag, nom, idens)VALUES (2, 'Marrakech', 1);

INSERT INTO magasin( idmag, nom, idens)VALUES (3, 'Rabat', 1);

INSERT INTO magasin( idmag, nom, idens)VALUES (4, 'Safi', 2);

INSERT INTO magasin( idmag, nom, idens)VALUES (5, 'Rabat', 2);

INSERT INTO magasin( idmag, nom, idens)VALUES (6, 'casablanca', 2);

INSERT INTO magasin( idmag, nom, idens)VALUES (7, 'Laarach', 2);

INSERT INTO magasin( idmag, nom, idens)VALUES (8, 'Rabat', 3);

INSERT INTO magasin( idmag, nom, idens)VALUES (9, 'Marrakech', 3);


-- objectifs

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (1, 1,1, 2016, 1200, 50, '10/10/2016', '10/10/2016', 'teest', 'test', true, 10000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (2, 4,2, 2016, 1500, 76, '10/10/2016', '10/10/2016', 'admin', 'test1', true, 16000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (3, 8,3, 2015, 900, 30, '10/10/2016', '10/10/2016', 'admin', 'test2', false, 8000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (4, 3,1, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (5, 5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (6, 9,3, 2015, 2300, 80, '10/10/2016', '10/10/2016', 'mouad', 'test5', true, 25600);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (7, 7,2, 2015, 2000, 59, '10/10/2016', '10/10/2016', 'saad', 'test6', true, 23200);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (1, 5,2, 2016, 1200, 50, '10/10/2016', '10/10/2016', 'teest', 'test', true, 10000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (3, 5,2, 2016, 1500, 76, '10/10/2016', '10/10/2016', 'admin', 'test1', true, 16000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (8, 5,2, 2015, 900, 30, '10/10/2016', '10/10/2016', 'admin', 'test2', false, 8000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (11, 5,2, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (18,5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (21, 5,2, 2015, 2300, 80, '10/10/2016', '10/10/2016', 'mouad', 'test5', true, 25600);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (24, 5,2, 2015, 2000, 59, '10/10/2016', '10/10/2016', 'saad', 'test6', true, 23200);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (21, 5,2, 2016, 1200, 50, '10/10/2016', '10/10/2016', 'teest', 'test', true, 10000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (24, 5,2, 2016, 1500, 76, '10/10/2016', '10/10/2016', 'admin', 'test1', true, 16000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (25, 5,2, 2016, 900, 30, '10/10/2016', '10/10/2016', 'admin', 'test2', false, 8000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (26, 5,2, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (28,5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (35, 5,2, 2016, 2300, 80, '10/10/2016', '10/10/2016', 'mouad', 'test5', true, 25600);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (44, 5,2, 2016, 2000, 59, '10/10/2016', '10/10/2016', 'saad', 'test6', true, 23200);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (6, 5,2, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (12,5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (10, 5,2, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (14,5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (48, 5,2, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (15,5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);
    
INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (2, 5,2, 2016, 1000, 28, '10/10/2016', '10/10/2016', 'mouad', 'test3', false, 8900);

INSERT INTO objectifs(ids, idmag, idens, an, pm, ncli, dt, dtm, usr, comment, vise, ca)
    VALUES (4,5,2, 2016, 1120, 40, '10/10/2016', '10/10/2016', 'admin', 'test4', false, 97000);
