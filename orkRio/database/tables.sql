-- Sequence: obj_id_seq
-- DROP SEQUENCE obj_id_seq;
CREATE SEQUENCE obj_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE objectifs
(
  ids smallint NOT NULL,
  idmag integer NOT NULL,
  idens integer NOT NULL,
  an integer NOT NULL,
  pm numeric,
  ncli integer,
  dt date,
  dtm date,
  usr character varying(40) NOT NULL,
  comment text,
  vise boolean,
  ca numeric NOT NULL,
  idobj integer DEFAULT nextval('obj_id_seq'::regclass),
  CONSTRAINT pk_obj PRIMARY KEY (ids, idens, idmag, an)
)
WITH (
  OIDS=FALSE
);

--Table : ENSEIGNE
CREATE TABLE enseigne
(
  idens integer NOT NULL,
  nom character varying(20),
  CONSTRAINT pk_idens PRIMARY KEY (idens)
)
WITH (
  OIDS=FALSE
);

-- Table: MAGASIN
CREATE TABLE magasin
(
  idmag integer NOT NULL,
  nom character varying(20),
  idens integer,
  CONSTRAINT pk_idmag PRIMARY KEY (idmag),
  CONSTRAINT fk_mag FOREIGN KEY (idens)
      REFERENCES enseigne (idens) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
  
